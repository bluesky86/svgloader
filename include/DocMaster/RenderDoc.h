// RenderDoc.h

#pragma once

#include "types.h"
#include "Matrix.h"
#include "Rect.h"
#include "Vector.h"

#define VECOTR_INCREMENT		5

namespace RenderDoc {

    class DLL_SO_EXPORT CObj
    {
    public:
        CObj();
        virtual ~CObj(){};
        virtual CObj* Copy();
        void CopyFrom(CObj *pObj);

        OBJ_TYPE objType;

    };

    class DLL_SO_EXPORT CRdColor //: public CObj
    {
    public:
        CRdColor();
        CRdColor(U8 r, U8 g, U8 b);
        CRdColor(U8 a, U8 r, U8 g, U8 b);
        ~CRdColor(){};
        virtual CRdColor* Copy();

        U8 GetRed();
        U8 GetGreen();
        U8 GetBlue();
        void SetColor(U32 color);
        void SetColor(U8 r, U8 g, U8 b);
        void GetColor(U8 &r, U8 &g, U8 &b);
        U32 GetColor();
        void operator = (const CRdColor &p);

    public:
        U32 color;
    };

    class DLL_SO_EXPORT CRdBrush //: public CObj
    {
    public:
        CRdBrush();
        CRdBrush(RD_BRUSH_STYLE style_, CRdColor &color_, ULong *hatch);
        ~CRdBrush();
        virtual CRdBrush* Copy();
        void CopyFrom(CRdBrush* pBrush);

        void GetColor(U8 &r, U8 &g, U8 &b);
        U32 GetColor();
        void SetColor(U8 r, U8 g, U8 b);
        void SetColor(U32 rgb);
        void GetHatchBkColor(U8 &r, U8 &g, U8 &b);
        U32 GetHatchBkColor();
        void SetHatchBkColor(U8 r, U8 g, U8 b);
        void SetHatchBkColor(U32 rgb);
        Float GetGradientAngle();
        void SetGradientAngle(Float angle);

    public:
        RD_BRUSH_STYLE style;
        CRdColor color;
        CRdColor hatchBkColor;
        Float gradientAngle;  // in degree, valide only if style is gradient, and mode is GRADIENT_Angle
        ULong *pHatch;		// brushStyle is BS_PATTERN, or BS_DIBPATTERN, or BS_DIBPATTERNPT, or BS_HATCHED
    };

    class DLL_SO_EXPORT CRdPen //: public CObj
    {
    public:
        CRdPen();
        ~CRdPen();
        virtual CRdPen* Copy();
        void CopyFrom(CRdPen* pPen);

        void GetPenColor(U8 &r, U8 &g, U8 &b);
        U32 GetPenColor();
        void SetPenColor(U8 r, U8 g, U8 b);
        void SetPenColor(U32 rgb);
        void GetBrushColor(U8 &r, U8 &g, U8 &b);
        U32 GetBrushColor();
        void SetBrushColor(U8 r, U8 g, U8 b);
        void SetBrushColor(U32 rgb);
        void GetHatchBkColor(U8 &r, U8 &g, U8 &b);
        U32 GetHatchBkColor();
        void SetHatchBkColor(U8 r, U8 g, U8 b);
        void SetHatchBkColor(U32 rgb);

    public:
        Bool isWeight; // true if the pen width is always 1 no matter how much it zooms.
        RD_PEN_STYLE style;
        RD_LS_ENDCAP_STYLE endCapStyle; // used when isWeight is false
        RD_LS_JOINT_STYLE jointStyle;   // used when isWeight is false
        S32 width; // always 1 if isWeight is true
        CRdColor color;
        RD_BRUSH_STYLE brushStyle;
        CRdColor brushColor;
        CRdColor hatchBkColor;
        ULong *pHatch;		// brushStyle is BS_PATTERN, or BS_DIBPATTERN, or BS_DIBPATTERNPT, or BS_HATCHED
        S32 numEntries;		// the number of entries of user-defined style
        S32 *styleEntry;	// user-defined style entries
    };

    class DLL_SO_EXPORT CDrawing : public CObj
    {
    public:
        CDrawing();
        virtual CDrawing* Copy();
        void CopyFrom(CDrawing *pObj);
        virtual ~CDrawing();
        virtual void Move(CRealPoint &delta); // the value delta is in parent space of this drawing object

        void* GetParams(){ return params; };

    public:
        CMatrix3x3 mat;

    protected: // render system use only
        void *params;
    };


    class DLL_SO_EXPORT CShape : public CDrawing
    {
    public:
        CShape();
        virtual ~CShape(){};
        void CopyFrom(CShape *pObj);
        virtual CShape* Copy();

        Bool penIsValid();
        Bool penBrushIsValid();

        CRdRect expendRect(CRdRect &r, RECT_OPERATION op = RECT_OP_AND);
        CRdRect expendRect(CShape &obj, RECT_OPERATION op = RECT_OP_AND);

    public:
        CRdPen pen;

        CRdRect rect;
    };

    class DLL_SO_EXPORT CPencilLine : public CShape
    {
    public:
        CPencilLine();
        CPencilLine(CRdColor color, S32 penWidth);
        virtual ~CPencilLine();
        void CopyFrom(CPencilLine *pObj);
        virtual CPencilLine* Copy();
        void SetPenColor(CRdColor color);
        void SetPenWidth(S32 width);
        void AppendPoint(CRealPoint pt);
        //void LineTo(CRealPoint pt);

    public:
        CVector<CRealPoint> points;
    };

    class DLL_SO_EXPORT CHighlighter : public CPencilLine
    {
    public:
        CHighlighter();
        CHighlighter(CRdColor color, S32 penWidth);
        virtual ~CHighlighter();
        void CopyFrom(CHighlighter *pObj);
        virtual CHighlighter* Copy();
        //void LineTo(CRealPoint pt);

    public:
        CVector<CRealPoint> points;
    };

    class DLL_SO_EXPORT CLine : public CShape
    {
    public:
        CLine();
        CLine(CRealPoint pt1, CRealPoint pt2);
        virtual ~CLine(){};
        void CopyFrom(CLine *pObj);
        virtual CLine* Copy();

    public:
        CRealPoint pt[2];
    };

    class DLL_SO_EXPORT CPolyline : public CShape
    {
    public:
        CPolyline();
        virtual ~CPolyline() {};
        void CopyFrom(CPolyline *pObj);
        virtual CPolyline* Copy();

        Bool isClosed();
        Bool setClosed();

    public:
        CPointList pts;
        CRdBrush brush;
        RD_FILL_MODE fillMode;
    };

    class DLL_SO_EXPORT CPolyPolyline : public CShape
    {
    public:
        CPolyPolyline();
        virtual ~CPolyPolyline();
        void CopyFrom(CPolyPolyline *pObj);
        virtual CPolyPolyline* Copy();

    public:
        CVector<CPolyline*> pline;
        CRdBrush brush;
        RD_FILL_MODE fillMode;
    };

    class DLL_SO_EXPORT CPolygon : public CShape
    {
    public:
        CPolygon();
        virtual ~CPolygon(){};
        void CopyFrom(CPolygon *pObj);
        virtual CPolygon* Copy();

    public:
        CPointList pts;
        CRdBrush brush;
        RD_FILL_MODE fillMode;
    };

    class DLL_SO_EXPORT CRectangle : public CShape
    {
    public:
        CRectangle();
        virtual ~CRectangle(){};
        void CopyFrom(CRectangle *pObj);
        virtual CRectangle* Copy();

    public:
        CRdBrush brush;
        CRdColor gradientColor;
        CRealPoint gradientVertex[3];
        S32 gradientMode; // 0 - horizontal, 1 - vertical, 2 - triangle
    };

    class DLL_SO_EXPORT CPolyPolygon : public CShape
    {
    public:
        CPolyPolygon();
        virtual ~CPolyPolygon();
        void CopyFrom(CPolyPolygon *pObj);
        virtual CPolyPolygon* Copy();

    public:
        CVector<CPolygon*> pgon;
        CRdBrush brush;
        RD_FILL_MODE fillMode;
    };

    class DLL_SO_EXPORT CPolyBezier : public CShape
    {
    public:
        CPolyBezier();
        virtual ~CPolyBezier(){};
        void CopyFrom(CPolyBezier *pObj);
        virtual CPolyBezier* Copy();

    public:
        CPointList pts;
        CRdBrush brush;
        RD_FILL_MODE fillMode;
    };

    class DLL_SO_EXPORT CAngleArc : public CShape
    {
    public:
        CAngleArc();
        virtual ~CAngleArc(){};
        void CopyFrom(CAngleArc *pObj);
        virtual CAngleArc* Copy();

    public:
        CRealPoint center;
        S32 radiusX, radiusY;
        Float startAngle; // in degree
        Float endAngle;   // in degree

        //CRdBrush brush;
    };

    class DLL_SO_EXPORT CArc : public CShape // ellipse arc including circle arc
    {
    public:
        CArc();
        virtual ~CArc(){};
        void CopyFrom(CArc *pObj);
        virtual CArc* Copy();

    public:
        CRealPoint startPoint;
        CRealPoint endPoint;
    };

    class DLL_SO_EXPORT CChord : public CArc
    {
    public:
        CChord();
        virtual ~CChord(){};
        void CopyFrom(CChord *pObj);
        virtual CChord* Copy();

    public:
        CRdBrush brush;
    };

    class DLL_SO_EXPORT CPie : public CChord
    {
    public:
        CPie();
        virtual ~CPie(){};
        void CopyFrom(CPie *pObj);
        virtual CPie* Copy();

    public:
        RD_FILL_MODE fillMode;
    };

    class DLL_SO_EXPORT CEllipse : public CShape
    {
    public:
        CEllipse();
        virtual ~CEllipse(){};
        void CopyFrom(CEllipse *pObj);
        virtual CEllipse* Copy();

    public:
        CRdBrush brush;
    };

    class DLL_SO_EXPORT CPath : public CShape
    {
    public:
        CPath();
        virtual ~CPath();
        void CopyFrom(CPath *pObj);
        virtual CPath* Copy();

        void MoveTo(CRealPoint pt);
        void LineTo(CRealPoint pt, Bool isEnd = false);
        void BezierTo(CRealPoint pt1, CRealPoint pt2, CRealPoint pt3, Bool isEnd = false);

    public:
        CPointList pts;
        CVector<RD_PATH_OP> opTypes;

        CRdBrush brush;
        RD_FILL_MODE fillMode;
    };

    class DLL_SO_EXPORT CClipPath : public CObj
    {
    public:
        CClipPath();
        virtual ~CClipPath();
        void CopyFrom(CClipPath *pObj);
        virtual CClipPath* Copy();

        void AddPath(CPath *pPath, RD_CLIP_MODE _mode = RD_CLIP_AND);
        void AddRect(CRdRect &rect, RD_CLIP_MODE _mode = RD_CLIP_AND);
        void AddShape(CShape *pShape, RD_CLIP_MODE _mode = RD_CLIP_AND);

    public:
        //CVector<CPath*> path;	// each path should be a closed path
        CVector<CShape*> rgn;	// each path should be a closed path
        CVector<RD_CLIP_MODE> mode;
    };

    class DLL_SO_EXPORT CRdImage : public CDrawing
    {
    public:
        CRdImage();
        ~CRdImage();
        void CopyFrom(CRdImage *pObj);
        virtual CRdImage* Copy();

        Bool checkBilevel(Bool forceReCheck = false);  // for 1-bit image only
        Bool checkGrayScale(Bool forceReCheck = false); // for 8-bit image, if it is a grayscale image, isGrayScale and grayScaleColorIndex will be set
        void convert8bitIndexTo8bitGrayScale();

        U8* AllocBits();
        U8* AllocBits(Long size);
        void DeleteBits();

    public:
        Long width;
        Long height;
        Long byteWidth;
        S16 planes;
        S16 bitCount;
        IMAGE_COMPRESSION compression;
        S32 sizeImage;

        Bool bDecoded; // if the data is decoded
        U8 *pData;
        Bool bIsRGB;  // data is in RGBA or BGRA, only when valid when pData is 24-bit or 32-bit, not palette
        Bool bFreeData;

        CRdColor *colorTable; // the number of color defined by bitCount
        S32 numColors;
        Bool hasTransparentColor;
        CRdColor transparentColor;
        U8 constantAlpha;	// for 32-bit image only

        Bool isBilevel;
        Bool isGrayScale;  // 8-bit or 1-bit image only
        U8 grayScaleColorIndex[2]; //8-bit image only, may not point to (0,0,0) and (255, 255, 255), can be any grayscale value

        //CRdRect rect;

    private:
        Bool bBitsIndexToColor;  // if the bits has been converted from color index to real color value for 8-bit grayscale image
        //Bool bBitsAllocated;
    };

    class DLL_SO_EXPORT CRdFont
    {
    public:
        CRdFont();
        void CopyFrom(CRdFont *pObj);
        virtual CRdFont* Copy();

    public:
        Bool isPointFont;
        Long point;
        Long  height;
        Long  width;
        Long  weight;			// RD_WEIGHT_ENUM, can be a value from 0 ~ 1000
        Byte  italic;
        Byte  underline;
        Byte  strikeOut;
        Byte  charSet;
        Byte  quality;			// RD_PITCHANDFAMILY_ENUM
        Byte  pitchAndFamily;	// RD_FONT_CAHRSET
        Wchar faceName[RD_FONT_FACE_SIZE]; // valid character length is 30
    };

    class DLL_SO_EXPORT CRdTextElem : public CDrawing
    {
    public:
        CRdTextElem();
        ~CRdTextElem();
        void CopyFrom(CRdTextElem *pObj);
        virtual CRdTextElem* Copy();

    public:
        CRdColor color;
        CRealPoint pos;
        Wchar *text;
        U32 len;
    };

    class DLL_SO_EXPORT CRdText : public CDrawing
    {
    public:
        CRdText();
        ~CRdText();
        void CopyFrom(CRdText *pObj);
        virtual CRdText* Copy();

        void SetFont(Long height, Long width, Wchar* faceName, Long weight = RD_FW_DONTCARE, Byte charSet = RD_ANSI_CHARSET, Byte quality = RD_FT_DEFAULT_QUALITY,
            Byte pitchAndFamily = DEFAULT_PITCH | RD_FF_DONTCARE, Byte italic = 0, Byte underline = 0, Byte strikeOut = 0);

        void SetFont(Long point, Wchar* faceName);
        void insertText(CRdColor color_, CRealPoint &pos, Wchar* text);

    public:
        CRdFont font;
        CVector<CRdTextElem*> textList;
    };

    class DLL_SO_EXPORT CRdWrapText : public CDrawing
    {
    public:
        CRdWrapText();
        ~CRdWrapText();
        void CopyFrom(CRdWrapText *pObj);
        virtual CRdWrapText* Copy();

        void clear();

        void SetFont(Long height, Long width, Wchar* faceName, Long weight = RD_FW_DONTCARE, Byte charSet = RD_ANSI_CHARSET, Byte quality = RD_FT_DEFAULT_QUALITY,
            Byte pitchAndFamily = DEFAULT_PITCH | RD_FF_DONTCARE, Byte italic = 0, Byte underline = 0, Byte strikeOut = 0);

        void SetFont(Long point, Wchar* faceName);
        void insertText(CRdColor color_, Wchar* text);
        void SetBbox(CRdRect bbox_);
        CRdRect* GetBbox(){ return &bbox; };

        // internal use only
        void* GetTextParams(){ return textParams; };

    public:
        CRdFont font;
        CRdColor color;
        Wchar *text;
        U32 len;
        CRdRect bbox;

    protected:
        void *textParams; // internal use only
    };

    typedef enum
    {
        COMMENTS_TAIL_JOINT_LEFT,
        COMMENTS_TAIL_JOINT_TOP,
        COMMENTS_TAIL_JOINT_RIGHT,
        COMMENTS_TAIL_JOINT_BOTTOM
    }COMMENTS_TAIL_JOINT;
    typedef enum
    {
        COMMENTS_SUBTYPE_ROUND_CONNER,
        COMMENTS_SUBTYPE_FLAT_CONNER,
        COMMENTS_SUBTYPE_SIMPLE_TAIL,
        COMMENTS_SUBTYPE_ROUND_BOX
    }COMMENTS_SUBTYPE;

    class DLL_SO_EXPORT CComments : public CShape
    {
    public:
        CComments();
        ~CComments();
        void CopyFrom(CComments *pObj);
        virtual CComments* Copy();


    public:
        COMMENTS_SUBTYPE subType;
        CRdWrapText wrapText;
        CRdRect bodyRect;
        CRealPoint tailPos;
        COMMENTS_TAIL_JOINT tailJoint;
    };

    //class DLL_SO_EXPORT CRef : public CObj
    //{
    //public:
    //    CRef();
    //    ~CRef();

    //public:
    //    CObj *m_pObj;

    //};

    class DLL_SO_EXPORT CGroup : public CShape
    {
    public:
        CGroup();
        ~CGroup();
        void CopyFrom(CGroup *pObj);
        virtual CGroup* Copy();

        Bool hasClipPath();
        void appendClipShape(CShape *pShape, RD_CLIP_MODE _mode = RD_CLIP_AND);
        void appendChild(CDrawing *obj);
        void deleteChild(CDrawing *obj);
        Bool detachChild(CDrawing *obj);
        void setBackGroundMode(GROUP_BACKGROUND_MODE mode);
        void setConstAlpha(U8 value);
        void setBkColor(CRdColor color);

    public:
        CClipPath clipPath;
        CVector<CDrawing*> children;
        U8 constAlpha;
        GROUP_BACKGROUND_MODE bkMode;
        CRdColor bkColor;
        //CVector<CObj*> refs; // a set of defs which referred by CRef object

        CRealPoint groupOffset; // Internal use only: for testpoint
        Bool bGroupRectAddedAsClip; // internal use
    };

    class DLL_SO_EXPORT CLayer : public CShape
    {
    public:
        CLayer();
        ~CLayer();
        CLayer(LAYER_TYPE _type);
        void appendClipShape(CShape *pShape, RD_CLIP_MODE _mode = RD_CLIP_AND);
        void appendChild(CDrawing *obj);
        void setBbox(CRdRect &_rect);
        void setBackGroundMode(GROUP_BACKGROUND_MODE mode);
        void setConstAlpha(U8 value);
        void setBkColor(CRdColor color);
        CGroup* getElements();
        CVector<CHighlighter*>* getHighlighters();
        void deleteChild(CDrawing *obj);

    public:
        LAYER_TYPE type;

    private:
        CVector<CHighlighter*> highLighters;
        CGroup elements;
    };

    class DLL_SO_EXPORT CRdPage : public CShape
    {
    public:
        CRdPage();
        ~CRdPage(); // will release all objects under it
        // loader should delete CRdPage object to release children

        S32 getLayerNum() { return nLayers; };
        Bool appendLayer(CLayer* layer);
        Bool insertLayer(S32 index, CLayer* layer);
        CLayer* getLayer(S32 index);

        void addFont(Wchar *faceName, Wchar *fileName); // add embedded font in page level, released when page releases
        void addFont(Wchar *faceName, U8 *fontData, S32 dataLen); // add embedded font in page level, released when page releases

    public:
        Handle fontList;

    private:
        CLayer *pLayer[MAX_LAYER_NUM];
        S32 nLayers;

    };

    class DLL_SO_EXPORT CRenderDoc : public CObj
    {
    public:
        CRenderDoc();
        ~CRenderDoc();

        void addFont(Wchar *faceName, Wchar *fileName); // add embedded font in doc level, released when doc closes
        void addFont(Wchar *faceName, U8 *fontData, S32 dataLen); // add embedded font in doc level, when after doc closes

    public:
        Wchar *fileName;
        Handle fontList;

        // point to an ID or a structure, or a class in loader.
        // WinRender won't used it, it is for loader to identify itself only.
        Handle hLoaderDocHandle;

    private:

    };

}
