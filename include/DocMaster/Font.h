#pragma once

#include "types.h"
#include "RenderDoc.h"
#include "Vector.h"


namespace RenderDoc
{
    class CEmbFont
    {
    public:
        CEmbFont();
        ~CEmbFont();

    public:
        Wchar faceName[RD_FONT_FACE_SIZE];
        Bool inMemory;
        Handle fontHandle;
        Wchar *fileName;
        U8 *data;
		U32 dataLen;
        Bool loaded;
    };

    typedef CVector<CEmbFont*> RdEmbFontList;

    class CEmbFontList
    {
    public:
        CEmbFontList();
        ~CEmbFontList();

        void insertFont(Wchar *faceName, Wchar *fileName);
        void insertFont(Wchar *faceName, U8 *fontData, S32 dataLen);
        S32 findFont(Wchar *faceName);

    public:
        RdEmbFontList fontList;

    };

};


