#pragma once

#include "types.h"
#include "matrix.h"

namespace RenderDoc {

    class DLL_SO_EXPORT lRect
    {
    public:
        lRect() :left(0), right(0), top(0), bottom(0){};
        int left, right, top, bottom;
    };
    class DLL_SO_EXPORT CRdRect
    {
    public:
        CRdRect();
        CRdRect(Float left, Float right, Float top, Float bottom);

    public:
        operator lRect() const
        {
            lRect result;
            result.left = (int)GetLeft();
            result.right = (int)GetRight();
            result.bottom = (int)GetBottom();
            result.top = (int)GetTop();
            return result;
        }

        CRdRect &operator = (const lRect rect)
        {
            SetLeft((Float)rect.left);
            SetRight((Float)rect.right);
            SetTop((Float)rect.top);
            SetBottom((Float)rect.bottom);
            return(*this);
        }

        CRdRect &operator = (const CRdRect rect)
        {
            SetLeft((Float)rect.GetLeft());
            SetRight((Float)rect.GetRight());
            SetTop((Float)rect.GetTop());
            SetBottom((Float)rect.GetBottom());
            return(*this);
        }


        void SetLeft(Float value){ m_left = value; m_bDirty = true; }
        void SetRight(Float value){ m_right = value; m_bDirty = true; }
        void SetBottom(Float value){ m_bottom = value; m_bDirty = true; }
        void SetTop(Float value){ m_top = value; m_bDirty = true; }
        Float GetLeft(void)const{ return m_left; }
        Float GetRight(void)const{ return m_right; }
        Float GetBottom(void)const{ return m_bottom; }
        Float GetTop(void)const{ return m_top; }
        Float GetWidth(void) const;
        Float GetHeight(void) const;
        Float GetArea(void) const;
        CRealPoint GetCenter(void) const;
        CRealPoint GetLL(void) const;
        CRealPoint GetLR(void) const;
        CRealPoint GetUL(void) const;
        CRealPoint GetUR(void) const;
        void Move(Float x, Float y);
        void Add(const CRealPoint &pt);
        void Set(const CRealPoint &pt);
        void Add(const CRdRect &Rect);
        Bool isOverlapped(const CRdRect *Rect);
        Bool isOverlapped(CRealPoint *p1, CRealPoint *p2);
        Bool isInside(CRealPoint *p);
        Bool isInside(CRdRect *Rect, Float tol = 0);
        Bool isOutside(CRdRect *Rect);
        Bool GetOverlap(CRdRect *rect, CRdRect *overlap);
        void Scale(Float percentage);
        Bool operator == (const CRdRect &otherrect) const;

        CRdRect &operator -= (Float val);
        CRdRect &operator += (Float val);

        void TransformBy(const CMatrix3x3 &mat);

        Float GetOverlapX(const CRdRect *rect) const;
        Float GetOverlapY(const CRdRect *rect) const;
        CRealVector GetDistanceTo(CRdRect &rect) const;
        void TrimTo(const CRdRect &rect);

        void TrimLineTo(CRealPoint *p1, CRealPoint *p2);

        Bool AND_me_with(CRdRect &rect);
        Bool OR_me_with(CRdRect &rect);
        Bool XOR_me_with(CRdRect &rect);
        Bool DIFF_me_with(CRdRect &rect);

	public:
		Float m_left;
		Float m_right;
		Float m_top;
		Float m_bottom;

    private:
        void CalcWidthAndHeight(void) const
        {
            m_width = (Float)fabs(m_right - m_left);
            m_height = (Float)fabs(m_top - m_bottom);
            m_bDirty = false;
        }

        mutable Bool   m_bDirty;
        mutable Float m_width;
        mutable Float m_height;
    };
};
