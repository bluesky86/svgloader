#pragma once

#include "types.h"
#include "RenderDoc.h"

// Loader returns true if this file can be parsed by this loader, otherwise false
// loader should check the file content to make sure it can be recognized.
// the function name in loader must be "Recognizer"
// "loader-config.xml" will list the file extensions each loader supported. Render will
// load the selected file with the loader registered in that xml. If it fails to load it,
// Render will loop each loader by calling this function, if any loader return true,
// "LoadDoc" of that loader will be called to load the file. And also it is possible, the 
// file don't have extension at all
typedef Bool (__stdcall *loader_Recognizer)(Wchar *fileName);
// recognizer from buffer. Name must be "RecognizerBuffer"
typedef Bool(__stdcall *loader_RecognizerBuffer)(U8 *pData, S32 datalen);

// Loader returns parsed contents in CRenderDoc. Render will release all contents within CRenderDoc
// after render them 
// the function name in loader must be "OpenDoc"
typedef S32 (__stdcall *loader_OpenDoc)(RenderDoc::CRenderDoc *renderDoc);

// load raster data from buffer. Name must be "LoadRdImage"
typedef RenderDoc::CRdImage* (__stdcall *loader_LoadRdImage)(U8 *pData, S32 datalen);

// the function name in loader must be "GetPage"
typedef RenderDoc::CRdPage* (__stdcall *loader_GetPage)(RenderDoc::CRenderDoc* renderDoc, S32 pageNo);

//// the function name in loader must be "DeletePage"
//typedef Bool(__stdcall *loader_DeletePage)(RenderDoc::CRenderDoc* renderDoc, RenderDoc::CRdPage* page);

// the function name in loader must be "CloseDoc"
typedef void(__stdcall *loader_CloseDoc)(RenderDoc::CRenderDoc* renderDoc);

// the function name in loader must be "WriteImage"
typedef Bool(__stdcall *loader_WriteImage)(RenderDoc::CRdImage* pRdImage, Wchar *fileName);

// the function name in loader must be "ConvertDoc"
typedef Bool(__stdcall *loader_ConvertDoc)(RenderDoc::CRenderDoc* renderDoc);

// If the function "LoadDoc" above return NULL, Render will call below function to retrieve
// error string. The function name must be "GetLoaderLastError"
// Loader has the responsibility to release the Wchar string, so it is better to define error
// string as a global variable and released when DLL is unloaded only.
typedef Wchar* (__stdcall *loader_GetLastError)();
