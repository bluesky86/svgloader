#pragma once

#include <fcntl.h>

#include <stdio.h>
#include "types.h"

#define FILENAME_SIZE      FILENAME_MAX
#define WFILENAME_SIZE     FILENAME_MAX
#define FILEBLOCK_SIZE     10240 // 10k
#define MAX_FILEBLOCK_SIZE 1048576 // 1meg

#define RD_FILE_FILE        0
#define RD_FILE_MEM         1

namespace RenderDoc
{

    typedef struct RD_FILE
    {
        S32                 type;  // memory file or file
        size_t              pos;  // current pos
        size_t              filesize;      // file or mem size
        Wchar               filename[FILENAME_SIZE];
        U8                  *buffer;
        FILE                *fp;
    } RD_FILE;

    DLL_SO_EXPORT RD_FILE*    rd_fopen(const S8 *filename, const S8 *mode, U8 *memBuf, size_t memBufSize);
    DLL_SO_EXPORT RD_FILE*    rd_wfopen(const Wchar *filename, const Wchar *mode, U8 *memBuf, size_t memBufSize);
    DLL_SO_EXPORT RD_FILE*    rd_wfopen_2mem(const Wchar *filename, const Wchar *mode);
    DLL_SO_EXPORT S32         rd_wfopenf(RD_FILE *bsFile, const Wchar *filename, const Wchar *mode, U8 *memBuf, size_t memBufSize);
    DLL_SO_EXPORT RD_FILE*    rd_createMemFile(size_t memBufSize);

    DLL_SO_EXPORT S32         rd_isMemFile(RD_FILE* fp);

    DLL_SO_EXPORT size_t      rd_flength(RD_FILE *fp);
    DLL_SO_EXPORT S8*         rd_fgets(S8 *string, S32 n, RD_FILE* fp); //fgets
    DLL_SO_EXPORT size_t      rd_fread(void *buffer, size_t size, size_t count, RD_FILE *fp);

    DLL_SO_EXPORT S32         rd_fprintf(RD_FILE* fp, const S8 *format, ...);
    DLL_SO_EXPORT S32         rd_fputc(S32 c, RD_FILE* fp);
    DLL_SO_EXPORT S32         rd_fputs(const S8 *string, RD_FILE* fp);
    DLL_SO_EXPORT size_t      rd_fwrite(const void *buffer, size_t size, size_t count, RD_FILE* fp);

    DLL_SO_EXPORT S32         rd_fclose(RD_FILE* fp, bool dataToo = true);
    DLL_SO_EXPORT S32         rd_feof(RD_FILE* fp);
    DLL_SO_EXPORT S32         rd_ferror(RD_FILE* fp);
    DLL_SO_EXPORT S32         rd_fflush(RD_FILE* fp);
    DLL_SO_EXPORT S32         rd_fseek(RD_FILE* fp, S32 offset, S32 origin);
    DLL_SO_EXPORT void        rd_rewind(RD_FILE* fp);
    DLL_SO_EXPORT S32         rd_ftell(RD_FILE* fp);
    DLL_SO_EXPORT S32         rd_setvbuf(RD_FILE* fp, char *buffer, S32 mode, size_t size);
    DLL_SO_EXPORT S32         rd_unlink(const S8 *filename);
    DLL_SO_EXPORT S32         rd_wunlink(const Wchar *filename);
    DLL_SO_EXPORT S32         rd_access(const Wchar *path, S32 mode);

}; // RenderDoc