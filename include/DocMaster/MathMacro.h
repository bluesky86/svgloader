#pragma once

#include "types.h"
#include "math.h"
#include <algorithm>

//namespace Model {

#undef PI
#define  PI                      ((Double)3.141592653589793238462643383279)

#undef PI2
#define  PI2                     ((Double)(2.0*PI))   

#if !defined(DEGREES360)
#define DEGREES360               ((Double)360.0)
#endif

#if !defined(DEG2RAD)
#define  DEG2RAD(a)              (((Double)(a))*(PI/(Float)180.0))
#endif

#if !defined(RAD2DEG)
#define  RAD2DEG(a)              (((Double)(a))*((Double)180.0/PI))
#endif

#if !defined(EPSILON)
#define  EPSILON                 ((Double)0.000001)
#endif

#if !defined(BSfloatMAX)
#define  FloatMAX               ((Double)3.402823466e+38)
#endif

	inline bool FEqual(Double x, Double y)
	{
		return (fabs((x)-(y)) < EPSILON);
	}

	inline bool FEqualEpsilon(Double x, Double y, Double epsilon)
	{
		return (fabs((x)-(y)) < epsilon);
	}

	inline bool FZero(Double x)
	{
		return (fabs(x) < EPSILON);
	}

	inline Double FSaneAngleRad(Double x)
	{
		return (x < (Float)0.0)
			? (PI2 + fmod((x + EPSILON), PI2) - EPSILON)
			: (fmod((x + EPSILON), PI2) - EPSILON);
	}

	inline Double FSaneAngleDeg(Double x)
	{
		return (x < 0.0)
			? DEGREES360 + fmod((x + EPSILON), DEGREES360) - EPSILON
			: fmod((x + EPSILON), DEGREES360) - EPSILON;
	}

	inline Double FAngleRadDiff(Double angleA, Double angleB)
	{
		Double absrad;
		angleA = FSaneAngleRad(angleA);
		angleB = FSaneAngleRad(angleB);

		if (angleA >= 0.0 && angleA < PI)
		{
			absrad = fabs(angleB - angleA);
			if (angleB >= PI)
			{
				angleB -= PI2;
				absrad = __min(fabs(angleB - angleA), absrad);
			}
		}
		else
		{
			absrad = fabs(angleB - angleA);
			if (angleB < PI)
			{
				angleB += PI2;
				absrad = __min(fabs(angleB - angleA), absrad);
			}
		}

		return absrad;
	}

//};
