#pragma once

#include <algorithm>
#include "MathMacro.h"
#include "RealPoint.h"
#include <vector>

namespace RenderDoc {

	class CMatrix3x3;

    // *ProtoComment*
    // Multiply the inverse of each matrix.

    DLL_SO_EXPORT CMatrix3x3 MatInvMult(const CMatrix3x3 &matrix0, const CMatrix3x3 &matrix1);

    // *ProtoComment*
    // Multiplication operator for 3x3 matrices

    DLL_SO_EXPORT CMatrix3x3 operator * (const CMatrix3x3 &matrix0, const CMatrix3x3 &matrix1);

    DLL_SO_EXPORT CMatrix3x3 operator * (const CMatrix3x3 &matrix, Float s);

    // *ProtoComment*
    // Scalar multiplication operator for 3x3 matrices

    DLL_SO_EXPORT CMatrix3x3 operator * (Float s, const CMatrix3x3 &matrix);


    // *ProtoComment*
    // Addition operator for 3x3 matrices

    DLL_SO_EXPORT CMatrix3x3 operator + (const CMatrix3x3 &matrix0, const CMatrix3x3 &matrix1);

    // *ProtoComment*
    // Subtraction operator for 3x3 matrices

    DLL_SO_EXPORT CMatrix3x3 operator - (const CMatrix3x3 &matrix0, const CMatrix3x3 &matrix1);

    // *ProtoComment*
    // Multiplies a point by a matrix and returns a point.

    DLL_SO_EXPORT CRealPoint operator * (const CRealPoint &p, const CMatrix3x3 &m);

    // *ProtoComment*
    // Multiplies a vector by a matrix and returns a vector. Note that the
    // translation portion of the matrix is ignored.

    DLL_SO_EXPORT CRealVector operator * (const CRealVector &v, const CMatrix3x3 &matrix);

    // *ProtoComment*
    // Builds a transformation matrix composed of rotations, translations,
    // and scalings. It is assumed that the rotations are given in radians.
    //
    // Although the result of the function is one 3x3 matrix, the result
    // is actually composed of five matrices in the following order.
    //
    //   $M_{t} * M_{s} * M_{x} * M_{y} * M_{z}$
    //
    // where $M_{t}$ is a translation matrix, $M_{s}$ is a scaling matrix, and
    // $M_{x}$, $M_{y}$, and $M_{z}$ are the rotation matrices about the
    // x-, y-, and z-axis respectively.

    DLL_SO_EXPORT CMatrix3x3 makeXForm(Double r = 0.0,
										Double tx = 0.0, Double ty = 0.0,
										Double sx = 1.0, Double sy = 1.0);

    DLL_SO_EXPORT Double det2x2(Double a00, Double a01, Double a10, Double a11);

    DLL_SO_EXPORT CRealPoint Interpolate(CRealPoint &p0, CRealPoint &p1, Float t);
    DLL_SO_EXPORT void Interpolate(CRealPoint &p0, CRealPoint &p1, Float t, CRealPoint &result);
    DLL_SO_EXPORT void InterpolateCubicBezier(CRealPoint *pPoints, Float t, CRealPoint &result);
    DLL_SO_EXPORT bool PointsAreCollinear(const CRealPoint &point1, const CRealPoint &point2, const CRealPoint &point3);
    DLL_SO_EXPORT void ClosestPointOnLineSegment(const CRealPoint & a, const CRealPoint & b, const CRealPoint & p1, CRealPoint &closest);
    DLL_SO_EXPORT Float ShortestDistance(const CRealPoint &b0, const CRealPoint &bn, const CRealPoint &mid);
    DLL_SO_EXPORT Float AbsAngleDiff(CRealVector &v1, CRealVector &v2);
    DLL_SO_EXPORT bool PerpIntersect(CRealPoint &line1, CRealPoint &line2, CRealPoint &p, CRealPoint &answer);
    DLL_SO_EXPORT void RemoveConsecutiveCoincidentalPoints(CRealPoint *pPoints, size_t numPoints, std::vector<CRealPoint> &cleaned);

    DLL_SO_EXPORT Float DotProduct(const CRealVector &v0, const CRealVector &v1);

    DLL_SO_EXPORT bool Compute3PointCircleCenter(CRealPoint &point1, CRealPoint &point2, CRealPoint &point3, CRealPoint &centerPoint);

    DLL_SO_EXPORT bool PointInsideConvex(CRealPoint &testpoint, CRealPoint *points, size_t numpoints);

    DLL_SO_EXPORT void OffsetPoints(const CRealPoint *inPoints, CRealPoint *outPoints, int size, Float offset);

	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
    template <class T>
    void matSwap(T &a, T &b)
    {
        T tmp(a);
        a = b;
        b = tmp;
    }

    class DLL_SO_EXPORT CMatrix3x3
    {

    public:

        // *MemberComment*
        // Default constructor. Does not initialize the elements.

        CMatrix3x3();

        // *MemberComment*
        // Constructor. Sets all elements in the matrix to the specified value.

        CMatrix3x3(Double value);

        // *MemberComment*
        // Copy constructor.

        CMatrix3x3(const CMatrix3x3 &rhs);

        // *MemberComment*
        // Assignment operator.

        CMatrix3x3 & operator = (const CMatrix3x3 &rhs);

        // *MemberComment*
        // Equality operator.

        bool operator == (const CMatrix3x3 &other) const;

        // *MemberComment*
        // Inverts the matrix provided that the matrix is nonsingular.

        bool Invert(void);

        bool InvertMat();

        // *MemberComment*
        // Inverts the matrix assuming the matrix is orthogonal.

        void InvertOrthogonal(void);

        // *MemberComment*
        // Return the determinant of the matrix.

        Double Determinant(void);

        // *MemberComment*
        // Transposes the matrix.

        void Transpose(void);

        // *MemberComment*
        // Set the matrix to an {\tt Identity} matrix.

        void SetIdentity(void);

        bool IsIdentity(void);

        bool SetInvert(void);

        void Set(Float m00, Float m01,
				Float m10, Float m11,
				Float m20, Float m21);

        void Get(Float &m00, Float &m01,
				Float &m10, Float &m11,
				Float &m20, Float &m21) const;

        bool IsOrthogonal(void);

        Float GetRotation() const;

        Float ScaleValue(Float value) const;

        Float RotateValue(Float value, bool bFlipDeviceY = false) const;


        void FindAdjoint(Double adjoint[3][3]);

        void operator += (CRealVector &p);

        // *MemberComment*
        // Overloaded function call operator. Returns the element at the
        // specified indices. No checking of indices is performed.

        Double &operator () (int i, int j);

        // *MemberComment*
        // Overloaded function call operator for const objects. Returns the
        // element at the specified indices. No checking of indices is performed.

        const Double &operator () (int i, int j) const;

        void MakeXForm(Double r = 0.0,
						Double tx = 0.0, Double ty = 0.0,
						Double sx = 1.0, Double sy = 1.0);

        void ExpandToIntegerSize(Double width, Double height);

        // *VarComment*
        // The elements of the matrix.

        Double mat[3][3];

        // *VarComment*
        // The inverse of the matrix (if it exists)

        Double inv[3][3];

        void SetDirty(void);
        void SetClean(void);

        mutable bool m_bHaveCalculatedScale;
        mutable bool m_bHaveCalculatedRotation;
        mutable Double m_rotation;
        mutable Double m_scale;
    }; //CMatrix3x3

};

