// Model.h

#pragma once

#include "types.h"
#include "math.h"
#include "Matrix.h"
#include "RealPoint.h"

namespace RenderDoc {


};

DLL_SO_EXPORT Wchar* RD_Char2Wchar(const char* src, Bool bFreeSrc = false);
DLL_SO_EXPORT size_t RD_Char2Wchar(Wchar* dest, size_t bufferLen, const char* src);
DLL_SO_EXPORT char* RD_Wchar2Char(const Wchar* src, Bool bFreeSrc = false);
DLL_SO_EXPORT size_t RD_Wchar2Char(char* dest, size_t bufferLen, const Wchar* src);
DLL_SO_EXPORT void RD_DeleteString(char *p);
DLL_SO_EXPORT void RD_DeleteString(Wchar *p);

