#pragma once

#include "types.h"
#include "math.h"

namespace RenderDoc {

	class CRealPoint;
	class DLL_SO_EXPORT CRealVector
    {
    public:
        CRealVector();
        CRealVector(Float iin, Float jin);
        CRealVector(const CRealPoint &p0, const CRealPoint &p1);

        Float I()const;
        Float J()const;
        void Set(Float iin, Float jin);
        Float Length();
        Double DblLength();
        void SetLength(Float newlength);
        Float Angle();
        Double DblAngle();
        void SetAngle(Float angle);
        void Rotate(Float angle);
        CRealVector &operator = (const CRealVector &p);

        CRealVector operator + (const CRealVector &p);
        CRealVector operator - (const CRealVector &p);

        CRealVector operator * (Float d);
        bool operator == (const CRealVector &p);
        bool operator != (const CRealVector &p);

        Float CrossProduct(const CRealVector &other);

    protected:
        Float i;
        Float j;
        Float length;
        Bool dirty;
    }; //CRealVector

	class DLL_SO_EXPORT CRealPoint
	{
	public:
		CRealPoint();
		CRealPoint(Float xin, Float yin);

		void AddX(Float inx);
		void AddY(Float iny);
        void Set(Float xin, Float yin);

        Float DistanceTo(const CRealPoint & pt) const;

        bool operator == (const CRealPoint &other) const;
        bool operator != (const CRealPoint &other) const;
        CRealPoint &operator = (const CRealPoint &p);
        void operator += (const CRealPoint &p);
        void operator -= (const CRealPoint &p);
        CRealPoint operator + (const CRealPoint &p) const;
        CRealPoint operator - (const CRealPoint &p) const;
        void operator += (const CRealVector &p);
        CRealPoint operator + (const CRealVector &p) const;
        CRealPoint operator - (const CRealVector &p) const;
        CRealPoint operator * (Float f) const;
        CRealPoint operator / (Float f) const;
        void operator *= (Float &f);
        void operator /= (Float &f);

	public:
        Float x;
        Float y;
	};

    class DLL_SO_EXPORT CPointList
    {
    public:
        CPointList();
        CPointList(const CPointList &otherList);
        virtual ~CPointList();
		void CopyFrom(CPointList* pts);

        void SetPoint(int index, const CRealPoint &point);
        void SetPoint(int index, CRealPoint *pPoint);
        void AddPoint(const CRealPoint &point);

        void AddPoint(const CRealPoint *pPoint);
        void PopLastPoint(void);

        CRealPoint *GetPoints(void);
        int GetPointCount(void) const { return m_pointcount; }

        void PFloatloc(int numpoints);

        void Clear(void)
        {
            m_pointcount = 0;
        }

        bool operator < (const CPointList& /*t*/) const
        {
            return false;
        }
        bool operator == (const CPointList& /*t*/) const
        {
            return false;
        }

        CRealPoint &operator [] (int index) const;

        bool DoesSelfIntersect(bool bClosedShape);

        Double Area(void); // assumes the pointlist is a closed shape

    protected:
        void Resize(int newpointcount);

        bool DoEdgesIntersect(CRealPoint *pEdge1Begin,
                            CRealPoint *pEdge1End,
                            CRealPoint *pEdge2Begin,
                            CRealPoint *pEdge2End);

        CRealPoint *m_pPoints;
        int           m_pointcount;
        int           m_pointsallocated;
    }; //CPointList

};
