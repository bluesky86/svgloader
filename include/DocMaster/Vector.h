#pragma once

#include "types.h"

const int _DEFAULT_VECTOR_SIZE = 4;

namespace RenderDoc
{
	template <typename T>
	class DLL_SO_EXPORT CVector
	{
	public:

		typedef T * iterator;

		CVector();
		CVector(U32 size);
		CVector(U32 size, const T &initial);
		CVector(const CVector<T> &v);
		~CVector();
		void CopyFrom(const CVector<T> *pVector);

		U32 capacity() const;
		U32 size() const;
		Bool empty() const;
		iterator begin();
		iterator end();
		T & front();
		T & back();
		void push_back(const T & value); 
		void pop_back(); 
		S32 find(T v);
		void pop(S32 index);

		void reserve(U32 capacity);   
		void resize(U32 size);   

		T & operator[](U32 index);  
		CVector<T> & operator=(const CVector<T> &);
		void clear();

	private:
		T *array_;
		U32 size_;
		U32 reserved_size_;
	};

	template <typename T>
	CVector<T>::CVector()
	{
		 array_ = new T[_DEFAULT_VECTOR_SIZE];
		 reserved_size_ =  _DEFAULT_VECTOR_SIZE;
		 size_ = 0;
	}

	template <typename T>
	CVector<T>::CVector(U32 size)
	{
		 array_ = new T[size];
		 reserved_size_ =  size;
		 size_ = 0;
	}

	template <typename T>
	CVector<T>::CVector(U32 size, const T &initial)
	{
		size_ = size;
		reserved_size_ = size;
		array_ = new T [size];
		for (S32 i = 0; i < size; i++)
			memcpy(&array_[i], &initial, sizeof(T));  
			//array_[i] = initial;
	}

	template<class T>
	CVector<T>::CVector(const CVector<T> & v)
	{
		size_ = v.size_;
		reserved_size_ = v.reserved_size_;
		array_ = new T[size_];  
		memcpy(array_, v.array_, size_ * sizeof(T));  
		//for (U32 i = 0; i < size_; i++)
		//	array_[i] = v.array_[i];  
	}

	template <typename T>
	CVector<T>::~CVector()
	{
		delete[] array_;
	}

	template<class T>
	void CVector<T>::CopyFrom(const CVector<T> *pVector)
	{
		if (pVector)
		{
			resize(pVector->capacity());
			size_ = pVector->size();
			memcpy(array_, pVector->array_, size_ * sizeof(T));
		}
	}

	template<class T>
	CVector<T> & CVector<T>::operator = (const CVector<T> & v)
	{
		delete[ ] array_;
		size_ = v.size_;
		reserved_size_ = v.reserved_size_;
		array_ = new T [size_];
		//for (U32 i = 0; i < size_; i++)
			memcpy(array_, v.array_, size_ * sizeof(T));  
			//array_[i] = v.array_[i];
		return *this;
	}

	template<class T>
	typename CVector<T>::iterator CVector<T>::begin()
	{
		return array_;
	}

	template<class T>
	typename CVector<T>::iterator CVector<T>::end()
	{
		return array_ + size();
	}

	template<class T>
	T& CVector<T>::front()
	{
		return array_[0];
	}

	template<class T>
	T& CVector<T>::back()
	{
		return array_[size_ - 1];
	}

	template<class T>
	void CVector<T>::push_back(const T & v)
	{
		if (size_ >= reserved_size_)
			reserve(reserved_size_ +5);
		array_ [size_++] = v;
	}

	template<class T>
	void CVector<T>::pop_back()
	{
		size_--;
	}

	template<class T>
	S32 CVector<T>::find(T v)
	{
		for (U32 i = 0; i < size_; i++)
		{
			if (array_[i] == v)
				return (S32)i;
		}
		return -1;
	}

	template<class T>
	void CVector<T>::pop(S32 index)
	{
		if ((U32)index > size_ - 1) return;

		size_--;
		for (U32 i = index; i < size_; i++)
		{
			array_[i] = array_[i + 1];
		}
	}

	template<class T>
	void CVector<T>::reserve(U32 capacity)
	{
		if(array_ == 0)
		{
			size_ = 0;
			reserved_size_ = 0;
		}    
		T * Newbuffer = new T [capacity];
		U32 l_Size = capacity < size_ ? capacity : size_;

		memcpy(Newbuffer, array_, l_Size * sizeof(T));  
		//for (U32 i = 0; i < l_Size; i++)
		//	Newbuffer[i] = array_[i];


		reserved_size_ = capacity;
		delete[] array_;
		array_ = Newbuffer;
	}

	template<class T>
	U32 CVector<T>::size()const
	{
		return size_;
	}

	template<class T>
	void CVector<T>::resize(U32 size)
	{
		reserve(size);
		size_ = size;
	}

	template<class T>
	T& CVector<T>::operator[](U32 index)
	{
		return array_[index];
	}  

	template<class T>
	unsigned int CVector<T>::capacity()const
	{
		return reserved_size_;
	}

	template <class T>
	void CVector<T>::clear()
	{
		reserved_size_ = 0;
		size_ = 0;
        if (array_)
            delete[] array_;
		array_ = 0;
	}

	template <class T>
	Bool CVector<T>::empty() const
	{
		return (size_ == 0);
	}

};


