#pragma once

#define MAX_LAYER_NUM			7
#define FILEPATH_MAX_LENGTH     512
#define RD_FONT_FACE_SIZE       32

#define PAGE_MARGIN					3
#define PAGE_ZOOM_RATIO				0.2f
#define PAGE_MOUSE_ZOOM_RATIO		0.1f

typedef unsigned char U8;
typedef char S8;
typedef unsigned char Byte;
typedef unsigned short U16;
typedef short S16;
typedef int S32;
typedef unsigned int U32;
typedef float Float;
typedef double Double;
typedef unsigned long ULong;
typedef long Long;
typedef U16 Word;
typedef U32 Dword;
typedef void Void;
typedef void* Handle;
typedef bool Bool;
typedef wchar_t Wchar;

#define DLL_SO_EXPORT __declspec(dllexport)
#define DLL_SO_IMPORT __declspec(dllimport)

// file type
#define FILETYPE_PNG            1
#define FILETYPE_JPG            2
#define FILETYPE_TIF            3
#define FILETYPE_GIF            4

// color type
#define FILE_COLORTYPE_256              0
#define FILE_COLORTYPE_BILEVLE          1
#define FILE_COLORTYPE_GRAYSCALE        2
#define FILE_COLORTYPE_TRUECOLOR        3
#define FILE_COMPOUND_FILE              4


namespace RenderDoc {

	typedef enum
	{
		RECT_OP_AND,
		RECT_OP_OR,
		RECT_OP_XOR,
		RECT_OP_DIFF
	}RECT_OPERATION;

	typedef enum
	{
		RDPS_NULL,
		RDPS_SOLID,
		RDPS_DASH,
		RDPS_DASHDOT,
		RDPS_DASHDOTDOT,
		RDPS_DOT,
		RDPS_USERSTYLE
	}RD_PEN_STYLE;

	typedef enum
	{
		RDLS_ENDCAP_FLAT,
		RDLS_ENDCAP_ROUND,
		RDLS_ENDCAP_SQUARE
	}RD_LS_ENDCAP_STYLE;	// end cap of line style

	typedef enum
	{
		RDLS_JOINT_BEVEL,
		RDLS_JOINT_MITER,
		RDLS_JOINT_ROUND
	}RD_LS_JOINT_STYLE;		// joint style of line

	typedef enum
	{
		RDBS_NULL,
		RDBS_SOLID,
		RDBS_PATTERN,
		RDBS_HATCHED,
		RDBS_HOLLOW,
		RDBS_DIBPATTERN,	// An image
		RDBS_DIBPATTERNPT,
		RDBS_GRADIENT		// only for rectangle gradient fill
	}RD_BRUSH_STYLE;

	typedef enum
	{
		RDHS_HORIZONTAL,	//Horizontal hatch
		RDHS_VERTICAL,		//Vertical hatch
		RDHS_FDIAGONAL,		//45-degree downward hatch (left to right)
		RDHS_DIAGCROSS,		//45-degree crosshatch
		RDHS_CROSS,			//Horizontal and vertical crosshatch
		RDHS_BDIAGONAL		//45-degree upward hatch (left to right)
	}RD_HATCH_STYLE;	// when brush style is RDBS_HATCHED

    typedef enum
    {
        GRADIENT_Horizontal,
        GRADIENT_Vertical,
        GRADIENT_Triangle,
        GRADIENT_ForwardDiagonal,
        GRADIENT_BackwardDiagonal,
        GRADIENT_Angle
    }GRADIENT_MODE;

	typedef enum
	{
		RDPATH_MOVETO,
		RDPATH_LINETO,
		RDPATH_BEZIERTO,
		RDPATH_LINETO_ANDCLOSE,
		RDPATH_BEZIERTO_ANDCLOSE,
		RDPATH_ELLIPSE,
		RDPATH_RECT,
		RDPATH_CHORD
	}RD_PATH_OP;

	typedef enum
	{
		RD_CLIP_AND,	//The new clipping region combines the overlapping areas of the current clipping region and the region identified by hrgn
		RD_CLIP_COPY,   //The new clipping region is replaced by hrgn
		RD_CLIP_DIFF,   //The new clipping region combines the areas of the current clipping region with those areas excluded from the region identified by hrgn
		RD_CLIP_OR,		//The new clipping region combines the current clipping region and the region identified by hrgn.
		RD_CLIP_XOR		//The new clipping region combines the current clipping region and the region identified by hrgn but excludes any overlapping areas.
	}RD_CLIP_MODE;

	typedef enum
	{
		GROUP_BK_MODE_COLOR,	// the background of group is a pure color, such as, white, black, other color.
		GROUP_BK_MODE_PARENT	//                         is the existing contents in parent obj within target rectangle
	} GROUP_BACKGROUND_MODE;

	typedef enum
	{
		LAYER_NORMAL,
		LAYER_MARKUP
	} LAYER_TYPE;

	typedef enum
	{
		RD_ODD_EVEN,
		RD_WINDING
	} RD_FILL_MODE;

	typedef enum
	{
		RD_RGB,		// uncompressed
		RD_RLE8,	// Run-length encode with 8bpp
		RD_RLE4,	// Run-length encode with 4bpp
		RD_JPEG,	// jpeg data
		RD_PNG		// png data
	}IMAGE_COMPRESSION;

    typedef enum
    {
        RD_FW_DONTCARE = 0,
        RD_FW_THIN = 100,
        RD_FW_EXTRALIGHT = 200,
        RD_FW_ULTRALIGHT = 200,
        RD_FW_LIGHT = 300,
        RD_FW_NORMAL = 400,
        RD_FW_REGULAR = 400,
        RD_FW_MEDIUM = 500,
        RD_FW_SEMIBOLD = 600,
        RD_FW_DEMIBOLD = 600,
        RD_FW_BOLD = 700,
        RD_FW_EXTRABOLD = 800,
        RD_FW_ULTRABOLD = 800,
        RD_FW_HEAVY = 900,
        RD_FW_BLACK = 900
    }RD_WEIGHT_ENUM;

    typedef enum
    {
        RD_ANSI_CHARSET,
        RD_BALTIC_CHARSET,
        RD_CHINESEBIG5_CHARSET,
        RD_DEFAULT_CHARSET,
        RD_EASTEUROPE_CHARSET,
        RD_GB2312_CHARSET,
        RD_GREEK_CHARSET,
        RD_HANGUL_CHARSET,
        RD_MAC_CHARSET,
        RD_OEM_CHARSET,
        RD_RUSSIAN_CHARSET,
        RD_SHIFTJIS_CHARSET,
        RD_SYMBOL_CHARSET,
        RD_TURKISH_CHARSET,
        RD_VIETNAMESE_CHARSET,
        RD_JOHAB_CHARSET,
        RD_ARABIC_CHARSET,
        RD_HEBREW_CHARSET,
        RD_THAI_CHARSET
    }RD_FONT_CHARSET;

	// font pitch
#define DEFAULT_PITCH           0
#define FIXED_PITCH             1
#define VARIABLE_PITCH          2

	// font family
#define RD_FF_DONTCARE         (0<<4)  /* Don't care or don't know. */
#define RD_FF_ROMAN            (1<<4)  /* Variable stroke width, serifed. */
	/* Times Roman, Century Schoolbook, etc. */
#define RD_FF_SWISS            (2<<4)  /* Variable stroke width, sans-serifed. */
	/* RD_FF_MODERN, Swiss, etc. */
#define FF_MODERN           (3<<4)  /* Constant stroke width, serifed or sans-serifed. */
	/* Pica, Elite, Courier, etc. */
#define RD_FF_SCRIPT           (4<<4)  /* Cursive, etc. */
#define RD_FF_DECORATIVE       (5<<4)  /* Old English, etc. */

    typedef enum
    {
        RD_FT_DEFAULT_QUALITY,
        RD_FT_ANTIALIASED_QUALITY
    }RD_FT_QUALITY;

    typedef enum
	{
		OBJTYPE_NONE,
		OBJTYPE_OBJ,
		OBJTYPE_DRAWING,
		OBJTYPE_SHAPE,
		OBJTYPE_LINE,
        OBJTYPE_PENCILLINE,
		OBJTYPE_HIGHLIGHTER,
		OBJTYPE_POLYLINE,
		OBJTYPE_POLYPOLYLINE,
		OBJTYPE_RECTANGLE,
		OBJTYPE_POLYGON,
		OBJTYPE_POLYPOLYGON,
		OBJTYPE_POLYBEZIER,
		OBJTYPE_ELLIPSE,
		OBJTYPE_ARC,
		OBJTYPE_ANGLE_ARC,
		OBJTYPE_PIE,
		OBJTYPE_CHORD,
		OBJTYPE_PATH,
		OBJTYPE_CLIP_PATH,
		OBJTYPE_GROUP,
        OBJTYPE_REF,
		OBJTYPE_IMAGE,
		OBJTYPE_TEXT,
		OBJTYPE_WRAPTEXT,
        OBJTYPE_COMMENTS,
		OBJTYPE_LAYER,
        OBJTYPE_PAGE,
        OBJTYPE_DOC
	}OBJ_TYPE;

};
