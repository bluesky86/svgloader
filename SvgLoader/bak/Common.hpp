#pragma once

#define BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS
#define BOOST_MPL_LIMIT_SET_SIZE 50

#define BOOST_PARAMETER_MAX_ARITY 17
// Following defines move parts of SVG++ code to svgpp_parser_impl.cpp file
// reducing compiler memory requirements
#define SVGPP_USE_EXTERNAL_PATH_DATA_PARSER
#define SVGPP_USE_EXTERNAL_TRANSFORM_PARSER
#define SVGPP_USE_EXTERNAL_PRESERVE_ASPECT_RATIO_PARSER
#define SVGPP_USE_EXTERNAL_PAINT_PARSER
#define SVGPP_USE_EXTERNAL_MISC_PARSER
#define SVGPP_USE_EXTERNAL_COLOR_PARSER
#define SVGPP_USE_EXTERNAL_LENGTH_PARSER

// for windows
#include <windows.h>
#undef min
#undef max
#undef small
#include <vector>

#include <svgpp/factory/integer_color.hpp>
#include <svgpp/factory/unitless_length.hpp>

#if defined(SVG_PARSER_RAPIDXML_NS)
# include "svgpp/src/demo/render/parser_rapidxml_ns.hpp"
#endif

#include <boost/function.hpp>
#include <boost/tuple/tuple.hpp>
#include <map>

// render doc
#include "types.h"
#include "RenderDoc.h"
#include "RealPoint.h"
#include "Matrix.h"

typedef double number_t;
typedef RenderDoc::CMatrix3x3 transform_t;
typedef RenderDoc::CRdColor color_t;
inline color_t BlackColor() { return color_t(0, 0, 0); }
inline color_t TransparentBlackColor() { return color_t(0, 0, 0); }
inline color_t TransparentWhiteColor() { return color_t(255, 255, 255); }
struct color_factory_base_t
{
    typedef RenderDoc::CRdColor color_type;

    static color_type create(unsigned char r, unsigned char g, unsigned char b)
    {
        return color_type(r, g, b);
    }
};
typedef svgpp::factory::color::percentage_adapter<color_factory_base_t> color_factory_t;

/////////////////////////
typedef svgpp::factory::length::unitless<number_t, number_t> length_factory_t;
typedef boost::tuple<double, double, double, double> bounding_box_t;
typedef boost::function<bounding_box_t()> get_bounding_box_func_t;

inline void AssignMatrix(RenderDoc::CMatrix3x3 & dest, RenderDoc::CMatrix3x3 const & src)
{
    dest = src;
}

class PathStorage
{
public:
    void path_move_to(number_t x, number_t y, svgpp::tag::coordinate::absolute const &)
    {
        path_points_.push_back(RenderDoc::CRealPoint((Float)x, (Float)y));
        path_types_.push_back(RenderDoc::RDPATH_MOVETO);
    }

    void path_line_to(number_t x, number_t y, svgpp::tag::coordinate::absolute const &)
    {
        path_points_.push_back(RenderDoc::CRealPoint((Float)x, (Float)y));
        path_types_.push_back(RenderDoc::RDPATH_LINETO);
    }

    void path_cubic_bezier_to(
        number_t x1, number_t y1,
        number_t x2, number_t y2,
        number_t x, number_t y,
        svgpp::tag::coordinate::absolute const &)
    {
        // TODO:
        path_points_.push_back(RenderDoc::CRealPoint((Float)x1, (Float)y1));
        path_types_.push_back(RenderDoc::RDPATH_BEZIERTO);
        path_points_.push_back(RenderDoc::CRealPoint((Float)x2, (Float)y2));
        path_types_.push_back(RenderDoc::RDPATH_BEZIERTO);
        path_points_.push_back(RenderDoc::CRealPoint((Float)x, (Float)y));
        path_types_.push_back(RenderDoc::RDPATH_BEZIERTO);
    }

    void path_close_subpath()
    {
        if (!path_types_.empty())
        {
            //path_types_.back() |= RenderDoc::RDPATH_CLOSE_PATH;
            path_points_.push_back(path_points_.front());
            path_types_.push_back(RenderDoc::RDPATH_LINETO);
        }
    }

    void path_exit()
    {}

    std::vector<RenderDoc::CRealPoint> path_points_;
    std::vector<RenderDoc::RD_PATH_OP> path_types_;
};

