#include "StdAfx.h"
#include "Matrix.h"

#include "clip_buffer.hpp"

#include <svgpp/svgpp.hpp>

ClipBuffer::ClipBuffer(int width, int height)
  : buffer_(width * height, 0xff)
  , width_(width), height_(height)
{}

ClipBuffer::ClipBuffer(ClipBuffer const & src)
  : buffer_(src.buffer_)
  , width_(src.width_), height_(src.height_)
{}

boost::gil::gray8c_view_t ClipBuffer::gilView() const
{
  return boost::gil::interleaved_view(width_, height_, 
    reinterpret_cast<const boost::gil::gray8_pixel_t *>(&buffer_[0]), width_);
}

void ClipBuffer::intersectClipRect(transform_t const & transform, number_t x, number_t y, number_t width, number_t height)
{
}

namespace 
{
  typedef boost::mpl::insert<
    svgpp::traits::shape_elements,
    svgpp::tag::element::use_
  >::type processed_elements;

  struct path_policy: svgpp::policy::path::no_shorthands
  {
    static const bool arc_as_cubic_bezier = true; 
    static const bool quadratic_bezier_as_cubic = true;
  };

  typedef boost::mpl::fold<
    svgpp::traits::shapes_attributes_by_element,
    boost::mpl::set<
      svgpp::tag::attribute::clip_rule,
      svgpp::tag::attribute::display,
      svgpp::tag::attribute::transform,
	    boost::mpl::pair<svgpp::tag::element::use_, svgpp::tag::attribute::xlink::href>,
	    boost::mpl::pair<svgpp::tag::element::use_, svgpp::tag::attribute::x>,
	    boost::mpl::pair<svgpp::tag::element::use_, svgpp::tag::attribute::y>
    >::type,
    boost::mpl::insert<boost::mpl::_1, boost::mpl::_2>
  >::type processed_attributes;

  class ElementBase
  {
  public:
    ElementBase(
      XMLDocument & xml_document,
      transform_t const & transform
      )
      : xml_document_(xml_document)
      , display_(true)
      , nonzero_clip_rule_(true)
    {
      AssignMatrix(transform_, transform);
    }

    ElementBase(ElementBase const & parent)
      : xml_document_(parent.xml_document_)
      , display_(parent.display_)
      , nonzero_clip_rule_(parent.nonzero_clip_rule_)
    {
      AssignMatrix(transform_, parent.transform_);
    }

    void set(svgpp::tag::attribute::display, svgpp::tag::value::none)
    { display_ = false; }

    void set(svgpp::tag::attribute::display, svgpp::tag::value::inherit)
    {}

    template<class ValueTag>
    void set(svgpp::tag::attribute::display, ValueTag)
    { display_ = true; }

    void set(svgpp::tag::attribute::clip_rule, svgpp::tag::value::nonzero)
    { nonzero_clip_rule_ = true; }

    void set(svgpp::tag::attribute::clip_rule, svgpp::tag::value::evenodd)
    { nonzero_clip_rule_ = false; }

    void transform_matrix(const boost::array<number_t, 6> & matrix)
    {
        //transform_.Multiply(&Gdiplus::Matrix(matrix[0], matrix[2], matrix[1], matrix[3], matrix[4], matrix[5]));

        RenderDoc::CMatrix3x3 mat;
        mat.Set((Float)matrix[0], (Float)matrix[2], (Float)matrix[1], (Float)matrix[3], (Float)matrix[4], (Float)matrix[5]);
        transform_ = transform_ * mat;
    }

  protected:
    XMLDocument & xml_document_;
    transform_t transform_;
    bool display_;
    bool nonzero_clip_rule_;
  };

  class Path: 
    public ElementBase
  , public PathStorage
  {
  public:
    Path(ElementBase const & parent)
      : ElementBase(parent)
    {}


    void on_exit_element()
    {
    }

  private:
  };

  class Use: public ElementBase
  {
  public:
    Use(ElementBase & parent)
      : ElementBase(parent)
    {}

    void on_exit_element();

    using ElementBase::set;

    template<class IRI>
    void set(svgpp::tag::attribute::xlink::href, svgpp::tag::iri_fragment, IRI const & fragment)
    { fragment_id_.assign(boost::begin(fragment), boost::end(fragment)); }

    template<class IRI>
    void set(svgpp::tag::attribute::xlink::href, IRI const & fragment)
    { std::cerr << "External references aren't supported\n"; }

    void set(svgpp::tag::attribute::x, number_t val)
    { x_ = val; }

    void set(svgpp::tag::attribute::y, number_t val)
    { y_ = val; }

  private:
    svg_string_t fragment_id_;
    number_t x_, y_;
  };

  struct context_factories
  {
    template<class ParentContext, class ElementTag>
    struct apply;
  };

  template<>
  struct context_factories::apply<ElementBase, svgpp::tag::element::use_>
  {
    typedef svgpp::factory::context::on_stack<Use> type;
  };

  template<class ElementTag>
  struct context_factories::apply<ElementBase, ElementTag>
  {
    typedef svgpp::factory::context::on_stack<Path> type;
  };

  length_factory_t length_factory_instance;

  struct length_policy_t
  {
    typedef length_factory_t const length_factory_type;

    static length_factory_type & length_factory(ElementBase const &)
    {
      return length_factory_instance;
    }
  };

  typedef svgpp::document_traversal<
    svgpp::number_type<Double>,
    svgpp::context_factories<context_factories>,
		svgpp::processed_elements<processed_elements>,
		svgpp::processed_attributes<processed_attributes>,
    svgpp::path_policy<path_policy>,
    svgpp::transform_events_policy<svgpp::policy::transform_events::forward_to_method<ElementBase> >,
    svgpp::length_policy<length_policy_t>
  > document_traversal;

  void Use::on_exit_element()
  {
    if (!display_)
      return;
    if (XMLElement element = xml_document_.findElementById(fragment_id_))
    {
      //transform_.Translate(x_, y_);
      transform_ = transform_ * RenderDoc::makeXForm(0.0, x_, y_);
      // TODO:
      document_traversal::load_referenced_element<
        svgpp::referencing_element<svgpp::tag::element::use_>,
        svgpp::expected_elements<svgpp::traits::shape_elements>
      >::load(element, static_cast<ElementBase &>(*this));
    }
    else
      std::cerr << "Element referenced by 'use' not found\n";
  }
}

void ClipBuffer::intersectClipPath(XMLDocument & xml_document, svg_string_t const & id, transform_t const & transform)
{
  if (XMLElement node = xml_document.findElementById(id))
  {
    try
    {
    } 
    catch (std::exception const & e)
    {
      std::cerr << "Error loading clipPath \"" << std::string(id.begin(), id.end()) << "\": " << e.what() << "\n";
    }
  }
}

