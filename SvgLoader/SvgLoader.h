#pragma once
#include "types.h"
#include "RenderDoc.h"
#include "SvgFile.h"

extern "C"
{
    DLL_SO_EXPORT Bool Recognizer(Wchar *fileName);

    DLL_SO_EXPORT S32 OpenDoc(RenderDoc::CRenderDoc* renderDoc);

    DLL_SO_EXPORT RenderDoc::CRdPage* GetPage(RenderDoc::CRenderDoc* renderDoc, S32 pageNo);

    DLL_SO_EXPORT void CloseDoc(RenderDoc::CRenderDoc* renderDoc);

    DLL_SO_EXPORT Wchar* GetLoaderLastError();

    DLL_SO_EXPORT Bool RecognizerBuffer(U8 *pData, S32 datalen);

    //DLL_SO_EXPORT RenderDoc::CRdImage* LoadRdImage(U8 *pData, S32 datalen);

    //DLL_SO_EXPORT Bool WriteImage(RenderDoc::CRdImage* pRdImage, Wchar *fileName);
};

extern size_t reverseCharPos(Wchar *string, Wchar ch, S32 startPos);

class CSvgLoader
{
public:
    CSvgLoader()
    {};

    ~CSvgLoader()
    {
        svgFile.Close();
    }

public:
    CSvgFile svgFile;

    RenderDoc::CRdPage *pRdPage;

private:
};
