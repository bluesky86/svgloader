#pragma once
#define BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS
#define BOOST_MPL_LIMIT_SET_SIZE 50

#include "types.h"
#include "RenderDoc.h"
#include "RdFile.h"

#include "common.hpp"

#include <svgpp/document_traversal.hpp>
#include <svgpp/utility/gil/mask.hpp>

#include <boost/bind.hpp>
#include <boost/gil/gil_all.hpp>
#include <boost/mpl/set.hpp>
#include <boost/mpl/transform_view.hpp>
#include <boost/optional.hpp>
#include <boost/scope_exit.hpp>


#include <map>
#include <set>
#include <fstream>
#include <numeric>

#include "stylable.hpp"
#include "gradient.hpp"
#include "clip_buffer.hpp"
#include "filter.hpp"

namespace boost {
    namespace mpl {

#   define BOOST_PP_ITERATION_PARAMS_1 \
    (3,(51, 60, <boost/mpl/set/aux_/numbered.hpp>))
#   include BOOST_PP_ITERATE()

    }
}


class Transformable;
class Canvas;
class Path;
class Use;
class Switch;
class ReferencedSymbolOrSvg;
class Mask;
class Marker;
//class ClipPath;

struct Document
{
    class FollowRef;

    Document(XMLDocument & xmlDocument)
        : xml_document_(xmlDocument)
        , gradients_(xml_document_)
        , filters_(xml_document_)
    {}

    XMLDocument & xml_document_;
    Gradients gradients_;
    Filters filters_;
    typedef std::set<XMLElement> followed_refs_t;
    followed_refs_t followed_refs_;
};

class Document::FollowRef
{
public:
    FollowRef(Document & document, XMLElement const & el)
        : document_(document)
    {
        std::pair<Document::followed_refs_t::iterator, bool> ins = document.followed_refs_.insert(el);
        if (!ins.second)
            throw std::runtime_error("Cyclic reference found");
        lock_ = ins.first;
    }

    ~FollowRef()
    {
        document_.followed_refs_.erase(lock_);
    }

private:
    Document & document_;
    Document::followed_refs_t::iterator lock_;
};

struct path_policy : svgpp::policy::path::no_shorthands
{
    static const bool arc_as_cubic_bezier = true;
    static const bool quadratic_bezier_as_cubic = true;
};

struct child_context_factories
{
    template<class ParentContext, class ElementTag, class Enable = void>
    struct apply;
};

template<>
struct child_context_factories::apply<Canvas, svgpp::tag::element::svg, void>
{
    typedef svgpp::factory::context::on_stack<Canvas> type;
};

template<>
struct child_context_factories::apply<Canvas, svgpp::tag::element::g, void>
{
    typedef svgpp::factory::context::on_stack<Canvas> type;
};

template<>
struct child_context_factories::apply<Canvas, svgpp::tag::element::a, void>
{
    typedef svgpp::factory::context::on_stack<Canvas> type;
};

template<>
struct child_context_factories::apply<Canvas, svgpp::tag::element::switch_, void>
{
    typedef svgpp::factory::context::on_stack<Switch> type;
};

template<class ElementTag>
struct child_context_factories::apply<Switch, ElementTag, void> : apply<Canvas, ElementTag>
{};

//template<>
//struct child_context_factories::apply<Canvas, svgpp::tag::element::clipPath, void>
//{
//    typedef svgpp::factory::context::on_stack<ClipPath> type;
//};
//
//template<class ElementTag>
//struct child_context_factories::apply<ClipPath, ElementTag, void> : apply<Canvas, ElementTag>
//{};
//
template<>
struct child_context_factories::apply<Canvas, svgpp::tag::element::use_, void>
{
    typedef svgpp::factory::context::on_stack<Use> type;
};

template<class ElementTag>
struct child_context_factories::apply<Canvas, ElementTag, typename boost::enable_if<boost::mpl::has_key<svgpp::traits::shape_elements, ElementTag> >::type>
{
    typedef svgpp::factory::context::on_stack<Path> type;
};

// Elements referenced by 'use' element
template<>
struct child_context_factories::apply<Use, svgpp::tag::element::svg, void>
{
    typedef svgpp::factory::context::on_stack<ReferencedSymbolOrSvg> type;
};

template<>
struct child_context_factories::apply<Use, svgpp::tag::element::symbol, void>
{
    typedef svgpp::factory::context::on_stack<ReferencedSymbolOrSvg> type;
};

template<class ElementTag>
struct child_context_factories::apply<Use, ElementTag, void> : child_context_factories::apply<Canvas, ElementTag>
{};

template<class ElementTag>
struct child_context_factories::apply<ReferencedSymbolOrSvg, ElementTag, void> : child_context_factories::apply<Canvas, ElementTag>
{};

// 'mask'
template<class ElementTag>
struct child_context_factories::apply<Mask, ElementTag, void> : apply<Canvas, ElementTag>
{};

// 'marker'
template<class ElementTag>
struct child_context_factories::apply<Marker, ElementTag, void> : child_context_factories::apply<Canvas, ElementTag>
{};

struct processed_elements
    : boost::mpl::set<
        svgpp::tag::element::svg,
        svgpp::tag::element::g,
        svgpp::tag::element::switch_,
        svgpp::tag::element::a,
        svgpp::tag::element::use_,
        svgpp::tag::element::path,
        svgpp::tag::element::rect,
        svgpp::tag::element::line,
        svgpp::tag::element::circle,
        svgpp::tag::element::ellipse,
        svgpp::tag::element::polyline,
        //svgpp::tag::element::clipPath,
        svgpp::tag::element::polygon
    >
{};

struct processed_attributes
    : boost::mpl::set58<
    // svgpp::traits::shapes_attributes_by_element
    boost::mpl::pair<svgpp::tag::element::path, svgpp::tag::attribute::d>,
    boost::mpl::pair<svgpp::tag::element::rect, svgpp::tag::attribute::x>,
    boost::mpl::pair<svgpp::tag::element::rect, svgpp::tag::attribute::y>,
    boost::mpl::pair<svgpp::tag::element::rect, svgpp::tag::attribute::width>,
    boost::mpl::pair<svgpp::tag::element::rect, svgpp::tag::attribute::height>,
    boost::mpl::pair<svgpp::tag::element::rect, svgpp::tag::attribute::rx>,
    boost::mpl::pair<svgpp::tag::element::rect, svgpp::tag::attribute::ry>,
    boost::mpl::pair<svgpp::tag::element::circle, svgpp::tag::attribute::cx>,
    boost::mpl::pair<svgpp::tag::element::circle, svgpp::tag::attribute::cy>,
    boost::mpl::pair<svgpp::tag::element::circle, svgpp::tag::attribute::r>,
    boost::mpl::pair<svgpp::tag::element::ellipse, svgpp::tag::attribute::cx>,
    boost::mpl::pair<svgpp::tag::element::ellipse, svgpp::tag::attribute::cy>,
    boost::mpl::pair<svgpp::tag::element::ellipse, svgpp::tag::attribute::rx>,
    boost::mpl::pair<svgpp::tag::element::ellipse, svgpp::tag::attribute::ry>,
    boost::mpl::pair<svgpp::tag::element::line, svgpp::tag::attribute::x1>,
    boost::mpl::pair<svgpp::tag::element::line, svgpp::tag::attribute::y1>,
    boost::mpl::pair<svgpp::tag::element::line, svgpp::tag::attribute::x2>,
    boost::mpl::pair<svgpp::tag::element::line, svgpp::tag::attribute::y2>,
    boost::mpl::pair<svgpp::tag::element::polyline, svgpp::tag::attribute::points>,
    boost::mpl::pair<svgpp::tag::element::polygon, svgpp::tag::attribute::points>,
    // svgpp::traits::viewport_attributes
    svgpp::tag::attribute::x,
    svgpp::tag::attribute::y,
    svgpp::tag::attribute::width,
    svgpp::tag::attribute::height,
    svgpp::tag::attribute::viewBox,
    svgpp::tag::attribute::preserveAspectRatio,
    //
    svgpp::tag::attribute::display,
    svgpp::tag::attribute::transform,
    svgpp::tag::attribute::clip_path,
    svgpp::tag::attribute::color,
    svgpp::tag::attribute::fill,
    svgpp::tag::attribute::fill_opacity,
    svgpp::tag::attribute::fill_rule,
    svgpp::tag::attribute::filter,
    svgpp::tag::attribute::marker_start,
    svgpp::tag::attribute::marker_mid,
    svgpp::tag::attribute::marker_end,
    svgpp::tag::attribute::marker,
    svgpp::tag::attribute::markerUnits,
    svgpp::tag::attribute::markerWidth,
    svgpp::tag::attribute::markerHeight,
    svgpp::tag::attribute::mask,
    svgpp::tag::attribute::maskUnits,
    svgpp::tag::attribute::maskContentUnits,
    svgpp::tag::attribute::refX,
    svgpp::tag::attribute::refY,
    svgpp::tag::attribute::stroke,
    svgpp::tag::attribute::stroke_width,
    svgpp::tag::attribute::stroke_opacity,
    svgpp::tag::attribute::stroke_linecap,
    svgpp::tag::attribute::stroke_linejoin,
    svgpp::tag::attribute::stroke_miterlimit,
    svgpp::tag::attribute::stroke_dasharray,
    svgpp::tag::attribute::stroke_dashoffset,
    svgpp::tag::attribute::opacity,
    svgpp::tag::attribute::orient,
    svgpp::tag::attribute::overflow,
    boost::mpl::pair<svgpp::tag::element::use_, svgpp::tag::attribute::xlink::href>
    >
{};

class ImageBuffer : boost::noncopyable
{
public:
    ImageBuffer()
    {}

    ImageBuffer(int width, int height)
    {
        setSize(width, height, TransparentBlackColor());
    }

    int width() const { return bitmap_->GetWidth(); }
    int height() const { return bitmap_->GetHeight(); }
    Gdiplus::Bitmap & bitmap() { return *bitmap_; }

    bool isSizeSet() const { return !buffer_.empty(); }

    void setSize(int width, int height, color_t const & fill_color)
    {
        BOOST_ASSERT(buffer_.empty());
        buffer_.resize(width * height * 4);
        bitmap_.reset(new Gdiplus::Bitmap(width, height, width * 4, PixelFormat32bppARGB, &buffer_[0]));
    }

    boost::gil::rgba8_view_t gilView()
    {
        return boost::gil::interleaved_view(bitmap_->GetWidth(), bitmap_->GetHeight(),
            reinterpret_cast<boost::gil::rgba8_pixel_t*>(&buffer_[0]), bitmap_->GetWidth() * 4);
    }

private:
    std::vector<BYTE> buffer_;
    std::unique_ptr<Gdiplus::Bitmap> bitmap_;
};

class Transformable
{
public:
    Transformable()
    {
        transform_[0] = transform_[3] = 1;
        transform_[1] = transform_[2] = transform_[4] = transform_[5] = 0;
    }

    Transformable(Transformable const & src)
    {
        transform_[0] = transform_[3] = 1;
        transform_[1] = transform_[2] = transform_[4] = transform_[5] = 0;
        AssignMatrix(transform_, src.transform_);
    }

    void transform_matrix(const boost::array<number_t, 6> & matrix)
    {
        RenderDoc::CMatrix3x3 mat1;
        mat1.Set((Float)matrix[0], (Float)matrix[1], (Float)matrix[2], (Float)matrix[3], (Float)matrix[4], (Float)matrix[5]);
        RenderDoc::CMatrix3x3 mat2;
        mat2.Set((Float)transform_[0], (Float)transform_[1], (Float)transform_[2], (Float)transform_[3], (Float)transform_[4], (Float)transform_[5]);
        mat2 = mat2 * mat1;
        transform_[0] = mat2(0, 0);
        transform_[1] = mat2(0, 1);
        transform_[2] = mat2(1, 0);
        transform_[3] = mat2(1, 1);
        transform_[4] = mat2(2, 0);
        transform_[5] = mat2(2, 1);

        //transform_.Multiply(
        //    &Gdiplus::Matrix(matrix[0], matrix[1], matrix[2], matrix[3], matrix[4], matrix[5]));
    }

    transform_t       & transform()       { return transform_; }
    transform_t const & transform() const { return transform_; }

private:
    transform_t transform_;
};

typedef boost::function<ImageBuffer&()> lazy_buffer_t;

class Canvas : public Stylable, public Transformable
{
public:
    struct dontInheritStyle {};

    Canvas(Document & document);

    Canvas(Document & document, ImageBuffer & image_buffer);

    Canvas(Canvas & parent);

    Canvas(Canvas & parent, dontInheritStyle);

    void on_exit_element();
    
    void ApplyClipPath();

    void set_viewport(number_t viewport_x, number_t viewport_y, number_t viewport_width, number_t viewport_height);

    void set_viewbox_size(number_t viewbox_width, number_t viewbox_height);

    void disable_rendering();

    bool rendering_disabled() const;

    length_factory_t & length_factory();

    length_factory_t const & length_factory() const;

private:
    Document & document_;
    ImageBuffer * const image_buffer_; // Non-NULL only for topmost SVG element
    lazy_buffer_t parent_buffer_;
    std::auto_ptr<ImageBuffer> own_buffer_;
    boost::shared_ptr<ClipBuffer> clip_buffer_;
    length_factory_t length_factory_;
    bool rendering_disabled_;

    void loadMask(ImageBuffer &) const;
    void applyFilter();
    ImageBuffer & getPassedImageBuffer() { return *image_buffer_; }

    number_t viewportWidth;
    number_t viewportHeight;

protected:
    ImageBuffer & getImageBuffer();

    Document & document() const { return document_; }
    ClipBuffer const & clipBuffer() const { return *clip_buffer_; }
    virtual bool isSwitchElement() const { return false; }
};

struct SimpleFilterView : IFilterView
{
    SimpleFilterView(boost::gil::rgba8c_view_t const & v)
        : view_(v)
    {}

    virtual boost::gil::rgba8c_view_t view() { return view_; }

private:
    boost::gil::rgba8c_view_t view_;
};

class Switch : public Canvas
{
public:
    Switch(Canvas & parent) : Canvas(parent)
    {}

    virtual bool isSwitchElement() const { return true; }
};

//class ClipPath : public Canvas
//{
//public:
//    ClipPath(Canvas & parent) : Canvas(parent)
//    {}
//
//    void on_exit_element()
//    {
//        int i = 0;
//    }
//};

class Path : public Canvas, public PathStorage
{
public:
    Path(Canvas & parent)
        : Canvas(parent)
    {}

    void on_exit_element();


    void marker(svgpp::marker_vertex v, number_t x, number_t y, number_t directionality, unsigned marker_index);

private:

    struct MarkerPos
    {
        svgpp::marker_vertex v;
        number_t x, y, directionality;
    };

    typedef std::vector<MarkerPos> Markers;
    Markers markers_;

    boost::optional<svg_string_t> & getMarkerReference(svgpp::marker_vertex v);

    typedef boost::variant<svgpp::tag::value::none, color_t, Gradient> EffectivePaint;
    void CreatePath();
    void CreateLinearGradient(LinearGradient const * linearGradient);
    void CreateRadialGradient(RadialGradient const * radialGradient);
    void drawMarkers();
    void drawMarker(svg_string_t const & id, number_t x, number_t y, number_t dir);
    EffectivePaint getEffectivePaint(Paint const &) const;

    Double GetVectorAngle(RenderDoc::CRealPoint &vstart, RenderDoc::CRealPoint &vend, RenderDoc::CRealPoint *pVertices, S32 outType, RenderDoc::CRealPoint &outStart, RenderDoc::CRealPoint &outEnd1, RenderDoc::CRealPoint &outEnd2);
};

struct afterMarkerUnitsTag {};

struct attribute_traversal : svgpp::policy::attribute_traversal::default_policy
{
    typedef boost::mpl::if_<
        boost::is_same<boost::mpl::_1, svgpp::tag::element::marker>,
        boost::mpl::vector<
        svgpp::tag::attribute::markerUnits,
        svgpp::tag::attribute::orient,
        svgpp::notify_context<afterMarkerUnitsTag>
        >::type,
        boost::mpl::empty_sequence
    > get_priority_attributes_by_element;
};

struct DocumentTraversalControl
{
    static bool proceed_to_element_content(Canvas const & canvas)
    {
        return canvas.style().display_ && !canvas.rendering_disabled();
    }

    template<class Context>
    static bool proceed_to_next_child(Context &)
    {
        return true;
    }
};

typedef
svgpp::document_traversal<
    svgpp::number_type<Double>,
    svgpp::context_factories<child_context_factories>,
    svgpp::length_policy<svgpp::policy::length::forward_to_method<Canvas, const length_factory_t> >,
    svgpp::color_factory<color_factory_t>,
    svgpp::processed_elements<processed_elements>,
    svgpp::processed_attributes<processed_attributes>,
    svgpp::path_policy<path_policy>,
    svgpp::document_traversal_control_policy<DocumentTraversalControl>,
    svgpp::transform_events_policy<svgpp::policy::transform_events::forward_to_method<Transformable> >, // Same as default, but less instantiations
    svgpp::path_events_policy<svgpp::policy::path_events::forward_to_method<Path> >, // Same as default, but less instantiations
    svgpp::error_policy<svgpp::policy::error::default_policy<Stylable> >, // Type of context isn't used
    svgpp::markers_policy<svgpp::policy::markers::calculate_always>,
    svgpp::attribute_traversal_policy<attribute_traversal>,
    svgpp::viewport_policy<svgpp::policy::viewport::as_transform>
> document_traversal_main;

class Use : public Canvas
{
public:
    Use(Canvas & parent);

    void on_exit_element();

    using Canvas::set;

    template<class IRI>
    void set(svgpp::tag::attribute::xlink::href, svgpp::tag::iri_fragment, IRI const & fragment)
    {
        fragment_id_.assign(boost::begin(fragment), boost::end(fragment));
    }

    template<class IRI>
    void set(svgpp::tag::attribute::xlink::href, IRI const & fragment)
    {
        std::cerr << "External references aren't supported\n";
    }

    void set(svgpp::tag::attribute::x, number_t val)
    {
        x_ = val;
    }

    void set(svgpp::tag::attribute::y, number_t val)
    {
        y_ = val;
    }

    void set(svgpp::tag::attribute::width, number_t val)
    {
        width_ = val;
    }

    void set(svgpp::tag::attribute::height, number_t val)
    {
        height_ = val;
    }

    boost::optional<number_t> const & width() const { return width_; }
    boost::optional<number_t> const & height() const { return height_; }

private:
    svg_string_t fragment_id_;
    number_t x_, y_;
    boost::optional<number_t> width_, height_;
};

class ReferencedSymbolOrSvg :
    public Canvas
{
public:
    ReferencedSymbolOrSvg(Use & parent)
        : Canvas(parent)
        , parent_(parent)
    {
    }

    void get_reference_viewport_size(number_t & width, number_t & height)
    {
        if (parent_.width())
            width = *parent_.width();
        if (parent_.height())
            height = *parent_.height();
    }

private:
    Use & parent_;
};

class Mask : public Canvas
{
public:
    Mask(Document & document, ImageBuffer & image_buffer, Transformable const & referenced)
        : Canvas(document, image_buffer)
        , maskUseObjectBoundingBox_(true)
        , maskContentUseObjectBoundingBox_(false)
    {
        AssignMatrix(transform(), referenced.transform());
    }

    void on_enter_element(svgpp::tag::element::mask)
    {}

    void on_exit_element()
    {}

    using Canvas::set;

    void set(svgpp::tag::attribute::maskUnits, svgpp::tag::value::userSpaceOnUse)
    {
        maskUseObjectBoundingBox_ = false;
    }

    void set(svgpp::tag::attribute::maskUnits, svgpp::tag::value::objectBoundingBox)
    {
        maskUseObjectBoundingBox_ = true;
    }

    void set(svgpp::tag::attribute::maskContentUnits, svgpp::tag::value::userSpaceOnUse)
    {
        maskContentUseObjectBoundingBox_ = false;
    }

    void set(svgpp::tag::attribute::maskContentUnits, svgpp::tag::value::objectBoundingBox)
    {
        maskContentUseObjectBoundingBox_ = true;
    }

    void set(svgpp::tag::attribute::x, number_t val)
    {
        x_ = val;
    }

    void set(svgpp::tag::attribute::y, number_t val)
    {
        y_ = val;
    }

    void set(svgpp::tag::attribute::width, number_t val)
    {
        width_ = val;
    }

    void set(svgpp::tag::attribute::height, number_t val)
    {
        height_ = val;
    }

private:
    bool maskUseObjectBoundingBox_, maskContentUseObjectBoundingBox_;
    number_t x_, y_, width_, height_; // TODO: defaults
};

struct GradientBase_visitor : boost::static_visitor<>
{
    void operator()(GradientBase const & g)
    {
        gradient_ = &g;
    }

    GradientBase const * gradient_;
};

class Marker :
    public Canvas
{
public:
    Marker(Path & parent, number_t strokeWidth, number_t x, number_t y, number_t autoOrient)
        : Canvas(parent, dontInheritStyle())
        , autoOrient_(autoOrient)
        , strokeWidth_(strokeWidth)
        , orient_(0.0)
        , strokeWidthUnits_(true)
    {
        transform()[4] += x;
        transform()[5] += y;
        //transform().Translate(x, y);
    }

    void on_enter_element(svgpp::tag::element::marker) {}
    void on_exit_element() {}

    bool notify(afterMarkerUnitsTag)
    {
        RenderDoc::CMatrix3x3 mat;
        mat.Set((Float)transform()[0], (Float)transform()[1], (Float)transform()[2], (Float)transform()[3], (Float)transform()[4], (Float)transform()[5]);

        // strokeWidthUnits_ and orient_ already set
        if (strokeWidthUnits_)
        {
            length_factory() = length_factory_t();
            mat = mat * RenderDoc::makeXForm(0, 0, 0, strokeWidth_, strokeWidth_);
            //transform().Scale(strokeWidth_, strokeWidth_);
        }
        //transform().Rotate(orient_ * boost::math::constants::radian<number_t>());
        mat = mat * RenderDoc::makeXForm(orient_ * boost::math::constants::radian<number_t>());
        transform()[0] = mat(0, 0);
        transform()[1] = mat(0, 1);
        transform()[2] = mat(1, 0);
        transform()[3] = mat(1, 1);
        transform()[4] = mat(2, 0);
        transform()[5] = mat(2, 1);

        return true;
    }

    using Canvas::set;

    void set(svgpp::tag::attribute::markerUnits, svgpp::tag::value::strokeWidth)
    {
        strokeWidthUnits_ = true;
    }

    void set(svgpp::tag::attribute::markerUnits, svgpp::tag::value::userSpaceOnUse)
    {
        strokeWidthUnits_ = false;
    }

    void set(svgpp::tag::attribute::orient, number_t val)
    {
        orient_ = val * boost::math::constants::degree<number_t>();
    }

    void set(svgpp::tag::attribute::orient, svgpp::tag::value::auto_)
    {
        orient_ = autoOrient_;
    }

private:
    number_t const strokeWidth_;
    number_t const autoOrient_;
    bool strokeWidthUnits_;
    number_t orient_;
};


extern char svg_doc_text[];

extern RenderDoc::CRdPage *g_SvgPage;
extern RenderDoc::CGroup *g_CurrentParent;
extern RenderDoc::CMatrix3x3 g_CurrentMatInv;
extern RenderDoc::CClipPath g_CurrentClipPath;
extern RenderDoc::RD_CLIP_MODE g_CurrentClipMode;

class CSvgppHandler
{
public:
    CSvgppHandler();
    ~CSvgppHandler();

public:
    Bool Open(const Wchar *fileName);
    Bool Open(const char *fileName);
    Bool Open(const char *pData, S32 buflen);

    Bool GetPageContent(RenderDoc::CRdPage *pPage, int pageNo = 0);

public:
    XMLDocument *m_pXmlDoc;

private:
    char *m_pFileName;
};
