//#include "stdafx.h"
#include "SvgppHandler.h"
#include "Matrix.h"
#include "MathMacro.h"


namespace
{
    template<class GrayMask>
    void blend_image_with_mask(boost::gil::rgba8_view_t const & rgbaView, GrayMask const & maskView)
    {
        boost::gil::rgba8_view_t::iterator o = rgbaView.begin();
        for (typename GrayMask::iterator m = maskView.begin(); m != maskView.end(); ++m, ++o)
        {
            using namespace boost::gil;
            get_color(*o, alpha_t()) =
                channel_multiply(
                get_color(*o, alpha_t()),
                get_color(*m, gray_color_t())
                );
        }
        BOOST_ASSERT(o == rgbaView.end());
    }
}

Canvas::Canvas(Document & document)
    : document_(document)
    , image_buffer_(NULL)
{
    viewportWidth = viewportHeight = 1;
}

Canvas::Canvas(Document & document, ImageBuffer & image_buffer)
    : document_(document)
    , parent_buffer_(boost::bind(&Canvas::getPassedImageBuffer, this))
    , image_buffer_(&image_buffer)
    , rendering_disabled_(false)
{
    viewportWidth = viewportHeight = 1;
    if (image_buffer.isSizeSet())
        clip_buffer_.reset(new ClipBuffer(image_buffer_->width(), image_buffer_->height()));
}

Canvas::Canvas(Canvas & parent)
    : Transformable(parent)
    , Stylable(parent)
    , document_(parent.document_)
    , image_buffer_(NULL)
    , parent_buffer_(boost::bind(&Canvas::getImageBuffer, &parent))
    , length_factory_(parent.length_factory_)
    , clip_buffer_(parent.clip_buffer_)
    , rendering_disabled_(false)
{
    viewportWidth = viewportHeight = 1;
}

Canvas::Canvas(Canvas & parent, dontInheritStyle)
    : Transformable(parent)
    , document_(parent.document_)
    , image_buffer_(NULL)
    , parent_buffer_(boost::bind(&Canvas::getImageBuffer, &parent))
    , length_factory_(parent.length_factory_)
    , clip_buffer_(parent.clip_buffer_)
    , rendering_disabled_(false)
{
    viewportWidth = viewportHeight = 1;
}

void Canvas::ApplyClipPath()
{
    if (style().clip_path_fragment_)
    {
        if (!clip_buffer_.unique())
            clip_buffer_.reset(new ClipBuffer(*clip_buffer_));
        clip_buffer_->intersectClipPath(document().xml_document_, *style().clip_path_fragment_, transform());
    }
}

void Canvas::on_exit_element()
{
    if (!own_buffer_.get())
        return;
    applyFilter();

    if (style().clip_path_fragment_)
    {
        if (!clip_buffer_.unique())
            clip_buffer_.reset(new ClipBuffer(*clip_buffer_));
        clip_buffer_->intersectClipPath(document().xml_document_, *style().clip_path_fragment_, transform());
    }

    if (clip_buffer_)
        blend_image_with_mask(own_buffer_->gilView(), clip_buffer_->gilView());

    if (style().mask_fragment_)
    {
        ImageBuffer & parent_buffer = parent_buffer_();
        ImageBuffer mask_buffer(parent_buffer.width(), parent_buffer.height());
        loadMask(mask_buffer);
        typedef boost::gil::color_converted_view_type<
            boost::gil::rgba8_view_t,
            boost::gil::gray8_pixel_t,
            svgpp::gil_utility::rgba_to_mask_color_converter<>
        >::type mask_view_t;
        mask_view_t mask_view = boost::gil::color_converted_view<boost::gil::gray8_pixel_t>(
            mask_buffer.gilView(), svgpp::gil_utility::rgba_to_mask_color_converter<>());

        blend_image_with_mask(own_buffer_->gilView(), mask_view);
    }
    {
        Gdiplus::ColorMatrix color_matrix[] = {
            1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, style().opacity_, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f, 1.0f };
        Gdiplus::ImageAttributes ImgAttr;
        ImgAttr.SetColorMatrix(color_matrix, Gdiplus::ColorMatrixFlagsDefault, Gdiplus::ColorAdjustTypeBitmap);
        Gdiplus::Graphics graphics(&parent_buffer_().bitmap());
        graphics.DrawImage(
            &own_buffer_->bitmap(),
            Gdiplus::Rect(0, 0, own_buffer_->width(), own_buffer_->height()),
            0, 0, own_buffer_->width(), own_buffer_->height(),
            Gdiplus::UnitPixel, &ImgAttr);
    }
}

void Canvas::set_viewport(number_t viewport_x, number_t viewport_y, number_t viewport_width, number_t viewport_height)
{
    if (image_buffer_) // If topmost SVG element
    {
        image_buffer_->setSize(viewport_width + 1.0, viewport_height + 1.0, TransparentWhiteColor());
        clip_buffer_.reset(new ClipBuffer(image_buffer_->width(), image_buffer_->height()));

        viewportWidth = viewport_width;
        viewportHeight = viewport_height;

        if (g_CurrentParent)
        {
            g_CurrentMatInv.SetIdentity();
            g_CurrentParent->rect = RenderDoc::CRdRect(viewport_x, viewport_width, viewport_y, viewport_height);
        }
    }
    else
    {
        if (style().overflow_clip_)
        {
            if (!clip_buffer_.unique())
                clip_buffer_.reset(new ClipBuffer(*clip_buffer_));
            clip_buffer_->intersectClipRect(transform(), viewport_x, viewport_y, viewport_width, viewport_height);
        }
    }
    length_factory_.set_viewport_size(viewport_width, viewport_height);
}

void Canvas::set_viewbox_size(number_t viewbox_width, number_t viewbox_height)
{
    // If "viewbox" attribute is present, then percentage is calculated relative to viewbox
    length_factory_.set_viewport_size(viewbox_width, viewbox_height);

    if (g_CurrentParent)
    {
        Double scale = 1;
        if (viewbox_width > viewbox_height)
            scale = viewportWidth / viewbox_width;
        else
            scale = viewportHeight / viewbox_height;

        g_CurrentMatInv = g_CurrentParent->mat * RenderDoc::makeXForm(0, (viewportWidth - viewbox_width * scale) / 2, (viewportHeight - viewbox_height * scale) / 2, scale, scale);
        g_CurrentMatInv.InvertMat();
        g_CurrentParent->rect = RenderDoc::CRdRect(0, viewbox_width, 0, viewbox_height);
    }
}

void Canvas::disable_rendering()
{
    rendering_disabled_ = true;
}

bool Canvas::rendering_disabled() const
{
    return rendering_disabled_;
}

length_factory_t & Canvas::length_factory()
{
    return length_factory_;
}

length_factory_t const & Canvas::length_factory() const
{
    return length_factory_;
}

void Canvas::applyFilter()
{
    if (!style().filter_)
        return;

    Filters::Input in;
    in.sourceGraphic_ = IFilterViewPtr(new SimpleFilterView(own_buffer_->gilView()));
    in.backgroundImage_ = IFilterViewPtr(new SimpleFilterView(parent_buffer_().gilView()));
    IFilterViewPtr out = document_.filters_.get(*style().filter_, length_factory_, in);
    if (out)
        boost::gil::copy_pixels(out->view(), own_buffer_->gilView());
}

void Canvas::loadMask(ImageBuffer & mask_buffer) const
{
    if (XMLElement element = document().xml_document_.findElementById(*style().mask_fragment_))
    {
        Document::FollowRef lock(document(), element);

        Mask mask(document_, mask_buffer, *this);
        document_traversal_main::load_expected_element(element, mask, svgpp::tag::element::mask());
    }
    else
        throw std::runtime_error("Element referenced by 'mask' not found");
}

ImageBuffer & Canvas::getImageBuffer()
{
    ImageBuffer & parent_buffer = parent_buffer_();

    if (style().opacity_ < 0.999
        || style().mask_fragment_
        || style().clip_path_fragment_
        || style().filter_)
    {
        if (!own_buffer_.get())
            own_buffer_.reset(new ImageBuffer(parent_buffer.width(), parent_buffer.height()));
        return *own_buffer_;
    }
    return parent_buffer;
}


void Path::on_exit_element()
{
    if (style().display_)
    {
        CreatePath();
        drawMarkers();
    }
    //Canvas::on_exit_element();
}

void Path::marker(svgpp::marker_vertex v, number_t x, number_t y, number_t directionality, unsigned marker_index)
{
    if (marker_index >= markers_.size())
        markers_.resize(marker_index + 1);
    MarkerPos & m = markers_[marker_index];
    m.v = v;
    m.x = x;
    m.y = y;
    m.directionality = directionality;
}

boost::optional<svg_string_t> & Path::getMarkerReference(svgpp::marker_vertex v)
{
    switch (v)
    {
    default:
        BOOST_ASSERT(false);
    case svgpp::marker_start:
        return style().marker_start_;
    case svgpp::marker_mid:
        return style().marker_mid_;
    case svgpp::marker_end:
        return style().marker_end_;
    }
}

#define _GRADIENT_H     0
#define _GRADIENT_V     1
#define _GRADIENT_T     2

//Double Path::GetVectorAngle(RenderDoc::CRealPoint &vstart, RenderDoc::CRealPoint &vend, RenderDoc::CRealPoint *pVertices, S32 outType, RenderDoc::CRealPoint &outStart, RenderDoc::CRealPoint &outEnd1, RenderDoc::CRealPoint &outEnd2)
//{
//    Double angle = 0;
//    Float cx = vend.x - vstart.x;
//    Float cy = vend.y - vstart.y;
//
//    outType = _GRADIENT_T;
//
//    if (cx == 0)
//    {
//        angle = PI / 2;
//        if (cy < 0)
//        {
//            angle += PI;
//            outStart = pVertices[3];
//            outEnd = pVertices[0];
//        }
//        else
//        {
//            outType = _GRADIENT_V;
//            outStart = pVertices[0];
//            outEnd = pVertices[3];
//        }
//    }
//    else if (cy == 0)
//    {
//        angle = 0;
//        if (cx < 0)
//        {
//            angle += PI;
//            outStart = pVertices[1];
//            outEnd = pVertices[0];
//        }
//        else
//        {
//            outType = _GRADIENT_H;
//            outStart = pVertices[0];
//            outEnd = pVertices[1];
//        }
//    }
//    else
//    {
//        angle = atan(cy / cx);
//        if (cx < 0)
//        {
//            angle += PI;
//        }
//        else
//        {
//
//        }
//    }
//    return angle;
//}

void Path::CreateLinearGradient(LinearGradient const * linearGradient)
{
    RenderDoc::CRectangle *pRectangle_ = NULL;
    Style &rStyle = style();
    U32 c, c1;

    c = linearGradient->stops_.front().color_;
    c1 = linearGradient->stops_.back().color_;

    RenderDoc::CRealPoint gradPt1 = RenderDoc::CRealPoint(linearGradient->x1_, linearGradient->y1_);
    RenderDoc::CRealPoint gradPt2 = RenderDoc::CRealPoint(linearGradient->x2_, linearGradient->y2_);

    //Double angle = GetVectorAngle(gradPt1, gradPt2);

    pRectangle_ = new RenderDoc::CRectangle();
    pRectangle_->brush.style = RenderDoc::RDBS_GRADIENT;
    pRectangle_->brush.SetColor(c);
    pRectangle_->gradientColor.SetColor(c1);
    pRectangle_->gradientVertex[0] = path_points_[0]; 
    pRectangle_->gradientVertex[1] = path_points_[2]; 


    if (linearGradient->x1_ == linearGradient->x2_) pRectangle_->gradientMode = 1;
    else pRectangle_->gradientMode = 0;

    transform_t &tran = transform();
    pRectangle_->mat.Set(tran[0], tran[1], tran[2], tran[3], tran[4], tran[5]);

    pRectangle_->mat = g_CurrentMatInv * pRectangle_->mat;

    RenderDoc::CRdRect rect;

    size_t size = path_points_.size();

    pRectangle_->rect.Set(path_points_[0]);
    for (size_t i = 1; i < size; i++)
    {
        pRectangle_->rect.Add(path_points_[i]);
    }


    Float penWidth = 1;

    EffectivePaint stroke = getEffectivePaint(rStyle.stroke_paint_);
    if (boost::get<svgpp::tag::value::none>(&stroke) == NULL)
    {
        if (color_t const * color = boost::get<color_t>(&stroke))
        {
            pRectangle_->pen.SetPenColor(*color);
            pRectangle_->pen.width = penWidth = rStyle.stroke_width_;
            pRectangle_->pen.endCapStyle = rStyle.line_cap_;
            pRectangle_->pen.jointStyle = rStyle.line_join_;
            //pen.SetMiterLimit(rStyle.miterlimit_);
            std::vector<number_t> const & dasharray = rStyle.stroke_dasharray_;
            if (dasharray.empty())
            {
                pRectangle_->pen.style = RenderDoc::RDPS_SOLID;
            }
            else
            {
                pRectangle_->pen.style = RenderDoc::RDPS_USERSTYLE;
                pRectangle_->pen.numEntries = dasharray.size();
                pRectangle_->pen.styleEntry = new S32[pRectangle_->pen.numEntries];
                for (int i = 0; i < pRectangle_->pen.numEntries; i++)
                {
                    pRectangle_->pen.styleEntry[i] = dasharray[i];
                }
            }
        }
        // TODO: gradient
    }

    penWidth = pRectangle_->mat.ScaleValue(penWidth) / 2;

    Float constAlpha = rStyle.opacity_ * rStyle.fill_opacity_;
    RenderDoc::CGroup *pGroup = NULL;

    // create new clip path
    g_CurrentClipPath.rgn.clear();
    g_CurrentClipPath.mode.clear();
    Canvas::ApplyClipPath();
    // create group for this path to have either clip path or const alpha
    if (g_CurrentClipPath.mode.size() > 0 || FEqual(constAlpha, 1.0) == false)
    {
        pGroup = new RenderDoc::CGroup();
        pGroup->rect = RenderDoc::CRdRect(rect.GetLeft() - penWidth, rect.GetRight() + penWidth, rect.GetTop() - penWidth, rect.GetBottom() + penWidth);
        pGroup->mat = pRectangle_->mat;
        pRectangle_->mat.SetIdentity();
        pGroup->appendChild(pRectangle_);
        if (g_CurrentClipPath.mode.size() > 0) // add clip path to new group
            pGroup->clipPath.CopyFrom(&g_CurrentClipPath);
        pGroup->constAlpha = (U8)(constAlpha * 255);
        g_CurrentParent->appendChild(pGroup);
    }
    else
        g_CurrentParent->appendChild(pRectangle_);

    g_CurrentClipPath.rgn.clear();
    g_CurrentClipPath.mode.clear();
}

void Path::CreateRadialGradient(RadialGradient const * radialGradient)
{

}

void Path::CreatePath()
{
    if (path_points_.empty()) return;

    RenderDoc::CPath *pPath_ = NULL;
    U32 c;

    Style &rStyle = style();

    EffectivePaint fill = getEffectivePaint(rStyle.fill_paint_);
    if (boost::get<svgpp::tag::value::none>(&fill) == NULL)
    {
        if (color_t const * color = boost::get<color_t>(&fill))
        {
            c = *color;
            pPath_ = new RenderDoc::CPath();
            pPath_->brush.style = RenderDoc::RDBS_SOLID;
            pPath_->brush.SetColor(c);
        }
        else if (Gradient const * gradient = boost::get<Gradient>(&fill))
        {
            if (LinearGradient const * linearGradient = boost::get<LinearGradient>(gradient))
            {
                CreateLinearGradient(linearGradient);
                return;
            }
            else if (RadialGradient const * radialGradient = boost::get<RadialGradient>(gradient))
            {
                CreateRadialGradient(radialGradient);
                return;
            }
        }
    }

    if (pPath_ == NULL) pPath_ = new RenderDoc::CPath();

    transform_t &tran = transform();
    pPath_->mat.Set(tran[0], tran[1], tran[2], tran[3], tran[4], tran[5]);

    pPath_->mat = g_CurrentMatInv * pPath_->mat;

    RenderDoc::CRdRect rect;

    size_t size = path_points_.size();

    for (size_t i = 0; i < size; i++)
    {
        if (i == 0) rect.Set(path_points_[i]);
        else rect.Add(path_points_[i]);

        switch (path_types_[i])
        {
        case RenderDoc::RDPATH_MOVETO:
            pPath_->MoveTo(path_points_[i]);
            break;
        case RenderDoc::RDPATH_LINETO:
            pPath_->LineTo(path_points_[i]);
            break;
        case RenderDoc::RDPATH_BEZIERTO:
            pPath_->BezierTo(path_points_[i], path_points_[i + 1], path_points_[i + 2]);
            i += 2;
            break;
        }
    }

    pPath_->fillMode = rStyle.nonzero_fill_rule_ ? RenderDoc::RD_WINDING : RenderDoc::RD_ODD_EVEN;


    Float penWidth = 1;

    EffectivePaint stroke = getEffectivePaint(rStyle.stroke_paint_);
    if (boost::get<svgpp::tag::value::none>(&stroke) == NULL)
    {
        if (color_t const * color = boost::get<color_t>(&stroke))
        {
            pPath_->pen.SetPenColor(*color);
            pPath_->pen.width = penWidth = rStyle.stroke_width_;
            pPath_->pen.endCapStyle = rStyle.line_cap_;
            pPath_->pen.jointStyle = rStyle.line_join_;
            //pen.SetMiterLimit(rStyle.miterlimit_);
            std::vector<number_t> const & dasharray = rStyle.stroke_dasharray_;
            if (dasharray.empty())
            {
                pPath_->pen.style = RenderDoc::RDPS_SOLID;
            }
            else
            {
                pPath_->pen.style = RenderDoc::RDPS_USERSTYLE;
                pPath_->pen.numEntries = dasharray.size();
                pPath_->pen.styleEntry = new S32[pPath_->pen.numEntries];
                for (int i = 0; i < pPath_->pen.numEntries; i++)
                {
                    pPath_->pen.styleEntry[i] = dasharray[i];
                }
            }
        }
        // TODO: gradient
    }

    penWidth = pPath_->mat.ScaleValue(penWidth) / 2;

    Float constAlpha = rStyle.opacity_ * rStyle.fill_opacity_;
    RenderDoc::CGroup *pGroup = NULL;

    // create new clip path
    g_CurrentClipPath.rgn.clear();
    g_CurrentClipPath.mode.clear();
    Canvas::ApplyClipPath();
    // create group for this path to have either clip path or const alpha
    if (g_CurrentClipPath.mode.size() > 0 || FEqual(constAlpha, 1.0) == false)
    {
        pGroup = new RenderDoc::CGroup();
        pGroup->rect = RenderDoc::CRdRect(rect.GetLeft() - penWidth, rect.GetRight() + penWidth, rect.GetTop() - penWidth, rect.GetBottom() + penWidth);
        pGroup->mat = pPath_->mat;
        pPath_->mat.SetIdentity();
        pGroup->appendChild(pPath_);
        if (g_CurrentClipPath.mode.size() > 0) // add clip path to new group
            pGroup->clipPath.CopyFrom(&g_CurrentClipPath);
        pGroup->constAlpha = (U8)(constAlpha * 255);
        g_CurrentParent->appendChild(pGroup);
    }
    else
        g_CurrentParent->appendChild(pPath_);

    g_CurrentClipPath.rgn.clear();
    g_CurrentClipPath.mode.clear();
}

void Path::drawMarkers()
{
    if (!style().marker_start_ && !style().marker_mid_ && !style().marker_end_)
        return;
    for (Markers::const_iterator pos = markers_.begin(); pos != markers_.end(); ++pos)
    {
        if (boost::optional<svg_string_t> & m = getMarkerReference(pos->v))
        {
            drawMarker(*m, pos->x, pos->y, pos->directionality);
        }
    }
}

void Path::drawMarker(svg_string_t const & id, number_t x, number_t y, number_t dir)
{
    if (XMLElement element = document().xml_document_.findElementById(id))
    {
        Document::FollowRef lock(document(), element);

        Marker markerContext(*this,
            style().stroke_width_,
            x, y, dir);
        document_traversal_main::load_expected_element(element, markerContext, svgpp::tag::element::marker());
    }
}

Path::EffectivePaint Path::getEffectivePaint(Paint const & paint) const
{
    SolidPaint const * solidPaint = NULL;
    if (IRIPaint const * iri = boost::get<IRIPaint>(&paint))
    {
        if (boost::optional<Gradient> const gradient = document().gradients_.get(iri->fragment_, length_factory()))
        {
            GradientBase_visitor gradientBase;
            boost::apply_visitor(gradientBase, *gradient);
            if (gradientBase.gradient_->stops_.empty())
                return svgpp::tag::value::none();
            if (gradientBase.gradient_->stops_.size() == 1)
                return gradientBase.gradient_->stops_.front().color_;
            if (LinearGradient const * linearGradient = boost::get<LinearGradient>(gradient.get_ptr()))
            {
                if (linearGradient->x1_ == linearGradient->x2_ && linearGradient->y1_ == linearGradient->y2_)
                    // TODO: use also last step opacity 
                    return gradientBase.gradient_->stops_.back().color_;
            }
            return *gradient;
        }
        if (iri->fallback_)
            solidPaint = &*iri->fallback_;
        else
            throw std::runtime_error("Can't find paint server");
    }
    else
        solidPaint = boost::get<SolidPaint>(&paint);
    if (boost::get<svgpp::tag::value::none>(solidPaint))
        return svgpp::tag::value::none();
    if (boost::get<svgpp::tag::value::currentColor>(solidPaint))
        return style().color_;
    return boost::get<color_t>(*solidPaint);
}


Use::Use(Canvas & parent)
    : Canvas(parent)
    , x_(0)
    , y_(0)
{}

void Use::on_exit_element()
{
    if (!style().display_)
        return;
    if (XMLElement element = document().xml_document_.findElementById(fragment_id_))
    {
        Document::FollowRef lock(document(), element);
        //transform().Translate(x_, y_);
        transform()[4] += x_;
        transform()[5] += y_;
        document_traversal_main::load_referenced_element<
            svgpp::referencing_element<svgpp::tag::element::use_>,
            svgpp::expected_elements<svgpp::traits::reusable_elements>,
            svgpp::processed_elements<
            boost::mpl::insert<processed_elements, svgpp::tag::element::symbol>::type
            >
        >::load(element, *this);
    }
    else
        std::cerr << "Element referenced by 'use' not found\n";
    Canvas::on_exit_element();
}

