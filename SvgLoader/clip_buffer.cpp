#include "clip_buffer.hpp"

#include <svgpp/svgpp.hpp>

#include "stylable.hpp"

#include "RenderDoc.h"
#include "Utils.h"

extern RenderDoc::CClipPath g_CurrentClipPath;
extern RenderDoc::RD_CLIP_MODE g_CurrentClipMode;

extern void SetError(const Wchar *msg);

ClipBuffer::ClipBuffer(int width, int height)
  : buffer_(width * height, 0xff)
  , width_(width), height_(height)
{}

ClipBuffer::ClipBuffer(ClipBuffer const & src)
  : buffer_(src.buffer_)
  , width_(src.width_), height_(src.height_)
{}

boost::gil::gray8c_view_t ClipBuffer::gilView() const
{
  return boost::gil::interleaved_view(width_, height_, 
    reinterpret_cast<const boost::gil::gray8_pixel_t *>(&buffer_[0]), width_);
}

void ClipBuffer::intersectClipRect(transform_t const & transform, number_t x, number_t y, number_t width, number_t height)
{
    int i = 0;
}


namespace
{
  typedef boost::mpl::insert<
    svgpp::traits::shape_elements,
    svgpp::tag::element::clipPath,
    svgpp::tag::element::use_
  >::type processed_elements;

  struct path_policy: svgpp::policy::path::no_shorthands
  {
    static const bool arc_as_cubic_bezier = true; 
    static const bool quadratic_bezier_as_cubic = true;
  };

  typedef boost::mpl::fold<
    svgpp::traits::shapes_attributes_by_element,
    boost::mpl::set<
        svgpp::tag::attribute::clip_rule,
        svgpp::tag::attribute::display,
        svgpp::tag::attribute::transform,
	    boost::mpl::pair<svgpp::tag::element::use_, svgpp::tag::attribute::xlink::href>,
	    boost::mpl::pair<svgpp::tag::element::use_, svgpp::tag::attribute::x>,
	    boost::mpl::pair<svgpp::tag::element::use_, svgpp::tag::attribute::y>,
        boost::mpl::pair<svgpp::tag::element::use_, svgpp::tag::attribute::y>/*,
        boost::mpl::pair<svgpp::tag::element::clipPath, svgpp::tag::attribute::clip_path>*/
    >::type,
    boost::mpl::insert<boost::mpl::_1, boost::mpl::_2>
  >::type processed_attributes;

  class ClipPath : public Stylable
  {
  public:
      ClipPath( XMLDocument & xml_document, transform_t const & transform )
          : xml_document_(xml_document)
      {
          AssignMatrix(transform_, transform);
      }

      void on_exit_element()
      {
          int i = 0;
      }

  protected:
      XMLDocument & xml_document_;
      transform_t transform_;
  };

  class ElementBase
  {
  public:
    ElementBase(
      XMLDocument & xml_document,
      transform_t const & transform
      )
      : xml_document_(xml_document)
      , display_(true)
      , nonzero_clip_rule_(true)
    {
      AssignMatrix(transform_, transform);
    }

    ElementBase(ElementBase const & parent)
      : xml_document_(parent.xml_document_)
      , display_(parent.display_)
      , nonzero_clip_rule_(parent.nonzero_clip_rule_)
    {
      AssignMatrix(transform_, parent.transform_);
    }

    void set(svgpp::tag::attribute::display, svgpp::tag::value::none)
    { display_ = false; }

    void set(svgpp::tag::attribute::display, svgpp::tag::value::inherit)
    {}

    template<class ValueTag>
    void set(svgpp::tag::attribute::display, ValueTag)
    { display_ = true; }

    void set(svgpp::tag::attribute::clip_rule, svgpp::tag::value::nonzero)
    { nonzero_clip_rule_ = true; }

    void set(svgpp::tag::attribute::clip_rule, svgpp::tag::value::evenodd)
    { nonzero_clip_rule_ = false; }

    template<class IRI>
    void set(svgpp::tag::attribute::clip_path, svgpp::tag::iri_fragment, IRI const & fragment)
    {
        clip_path_fragment_ = svg_string_t(boost::begin(fragment), boost::end(fragment));
    }

    void set(svgpp::tag::attribute::clip_path, svgpp::tag::value::none val)
    {
        clip_path_fragment_.reset();
    }

    void set(svgpp::tag::attribute::clip_path, svgpp::tag::value::inherit val)
    {
        //clip_path_fragment_ = parentStyle_.clip_path_fragment_;
        int i = 0;
    }

    void transform_matrix(const boost::array<number_t, 6> & matrix)
    {
        //transform_.Multiply(&Gdiplus::Matrix(matrix[0], matrix[2], matrix[1], matrix[3], matrix[4], matrix[5]));

        RenderDoc::CMatrix3x3 mat1;
        mat1.Set((Float)matrix[0], (Float)matrix[1], (Float)matrix[2], (Float)matrix[3], (Float)matrix[4], (Float)matrix[5]);
        RenderDoc::CMatrix3x3 mat2;
        mat2.Set((Float)transform_[0], (Float)transform_[1], (Float)transform_[2], (Float)transform_[3], (Float)transform_[4], (Float)transform_[5]);
        mat2 = mat2 * mat1;
        transform_[0] = mat2(0, 0);
        transform_[1] = mat2(0, 1);
        transform_[2] = mat2(1, 0);
        transform_[3] = mat2(1, 1);
        transform_[4] = mat2(2, 0);
        transform_[5] = mat2(2, 1);
    }

    void on_exit_element()
    {
        int i = 0;
    }

  protected:
    XMLDocument & xml_document_;
    transform_t transform_;
    bool display_;
    bool nonzero_clip_rule_;
    boost::optional<svg_string_t> clip_path_fragment_;
  };

  class Path: 
    public ElementBase
  , public PathStorage
  {
  public:
    Path(ElementBase const & parent)
      : ElementBase(parent)
    {}


    void on_exit_element()
    {
        int size = path_points_.size();
        if (size == 0) return;
        RenderDoc::CPath *pPath = new RenderDoc::CPath();
        for (int i = 0; i < size; i++)
        {
            switch (path_types_[i])
            {
            case RenderDoc::RDPATH_MOVETO:
                pPath->MoveTo(path_points_[i]);
                break;
            case RenderDoc::RDPATH_LINETO:
                pPath->LineTo(path_points_[i]);
                break;
            case RenderDoc::RDPATH_BEZIERTO:
                pPath->BezierTo(path_points_[i], path_points_[i + 1], path_points_[i + 2]);
                i += 2;
                break;
            }
        }
        // TODO: fill mode of styll
        //pPath->fillMode = style().nonzero_fill_rule_ ? RenderDoc::RD_WINDING : RenderDoc::RD_ODD_EVEN;
        pPath->fillMode = RenderDoc::RD_WINDING;
        if (g_CurrentClipPath.rgn.size() == 0)
            g_CurrentClipPath.AddPath(pPath, RenderDoc::RD_CLIP_COPY);
        else
            g_CurrentClipPath.AddPath(pPath, g_CurrentClipMode);
    }

  private:
  };

  class Use: public ElementBase
  {
  public:
    Use(ElementBase & parent)
      : ElementBase(parent)
    {}

    void on_exit_element();

    using ElementBase::set;

    template<class IRI>
    void set(svgpp::tag::attribute::xlink::href, svgpp::tag::iri_fragment, IRI const & fragment)
    { fragment_id_.assign(boost::begin(fragment), boost::end(fragment)); }

    template<class IRI>
    void set(svgpp::tag::attribute::xlink::href, IRI const & fragment)
    { std::cerr << "External references aren't supported\n"; }

    void set(svgpp::tag::attribute::x, number_t val)
    { x_ = val; }

    void set(svgpp::tag::attribute::y, number_t val)
    { y_ = val; }

  private:
    svg_string_t fragment_id_;
    number_t x_, y_;
  };

  struct context_factories
  {
    template<class ParentContext, class ElementTag>
    struct apply;
  };

  template<>
  struct context_factories::apply<ElementBase, svgpp::tag::element::use_>
  {
    typedef svgpp::factory::context::on_stack<Use> type;
  };

  template<class ElementTag>
  struct context_factories::apply<ElementBase, ElementTag>
  {
    typedef svgpp::factory::context::on_stack<Path> type;
  };

  template<>
  struct context_factories::apply<ElementBase, svgpp::tag::element::clipPath>
  {
      typedef svgpp::factory::context::on_stack<ClipPath> type;
  };

  length_factory_t length_factory_instance;

  struct length_policy_t
  {
    typedef length_factory_t const length_factory_type;

    static length_factory_type & length_factory(ElementBase const &)
    {
      return length_factory_instance;
    }
  };

  typedef svgpp::document_traversal<
        svgpp::number_type<Double >,
        svgpp::context_factories<context_factories>,
		svgpp::processed_elements<processed_elements>,
		svgpp::processed_attributes<processed_attributes>,
        svgpp::path_policy<path_policy>,
        svgpp::transform_events_policy<svgpp::policy::transform_events::forward_to_method<ElementBase> >,
        svgpp::length_policy<length_policy_t>
  > document_traversal;

  void Use::on_exit_element()
  {
    if (!display_)
      return;
    if (XMLElement element = xml_document_.findElementById(fragment_id_))
    {
        transform_[4] += x_;
        transform_[5] += y_;
      //transform_.Translate(x_, y_);
      //transform_ = transform_ * RenderDoc::makeXForm(0.0, x_, y_);
      // TODO:
        g_CurrentClipMode = RenderDoc::RD_CLIP_OR;
        document_traversal::load_referenced_element<
            svgpp::referencing_element<svgpp::tag::element::use_>,
            svgpp::expected_elements<svgpp::traits::shape_elements>
          >::load(element, static_cast<ElementBase &>(*this));
    }
    else
    {
        SetError(L"Element referenced by 'use' not found\n");
    }
  }
}

void ClipBuffer::intersectClipPath(XMLDocument & xml_document, svg_string_t const & id, transform_t const & transform)
{
  if (XMLElement node = xml_document.findElementById(id))
  {
    try
    {
        // use: union
        ElementBase root_context(xml_document, transform);
        document_traversal::load_expected_element(node, root_context, svgpp::tag::element::clipPath());

        // clip-path: intersection
        if (rapidxml_ns::xml_attribute<> const * id_attr = node->first_attribute("clip-path"))
        {
            char *pV = id_attr->value();
            S32 len = id_attr->value_size();
            if (len > 7 && strncmp(pV, "url(#", 5) == 0)
            {
                char *url = new char[len - 5];
                strncpy(url, &pV[5], len - 6);
                url[len - 6] = 0;
                if (XMLElement node1 = xml_document.findElementById(url))
                {
                    g_CurrentClipMode = RenderDoc::RD_CLIP_AND;
                    ElementBase root_context1(xml_document, transform);
                    document_traversal::load_expected_element(node1, root_context1, svgpp::tag::element::clipPath());
                }
                delete[] url;
            }
        }
    }
    catch (std::exception const & e)
    {
      char buf[256];
      sprintf_s(buf, 255, "Error loading clipPath \"%s\": %s\n", std::string(id.begin(), id.end()), e.what());
      Wchar *error = RD_Char2Wchar(buf);
      SetError(error);
      delete[] error;
    }
  }
}

