//#include "stdafx.h"
#include "SvgFile.h"
#include "SvgppHandler.h"

extern void SetError(const Wchar *msg);

CSvgFile::CSvgFile()
{
    svgppHandler = NULL;
}


CSvgFile::~CSvgFile()
{
}

Bool CSvgFile::Open(Wchar *filename, const Wchar *mode)
{
    if (svgppHandler) delete (CSvgppHandler*)svgppHandler;
    CSvgppHandler *pHandler = new CSvgppHandler();
    svgppHandler = pHandler;
    Bool res = pHandler->Open(filename);

    if (res == false)
    {
        SetError(L"Failed to open svg file. The file might not exist!");
        return false;
    }
    return true;
}

Bool CSvgFile::Open(U8 *pData, size_t bufLen)
{
    if (bufLen && pData)
    {

        if (svgppHandler) delete (CSvgppHandler*)svgppHandler;
        CSvgppHandler *pHandler = new CSvgppHandler();
        svgppHandler = pHandler;
        Bool res = pHandler->Open((const char*)pData, (S32)bufLen);

        if (res == false)
        {
            SetError(L"Failed to open svg file. The file might not exist!");
            return false;
        }
        return true;
    }
    SetError(L"The input data is empty!");
    return false;
}

Bool CSvgFile::GetPage(S32 pageNo, RenderDoc::CRdPage *pPage)
{
    if (pPage == NULL || svgppHandler == NULL) return false;

    CSvgppHandler *pHandler = (CSvgppHandler*)svgppHandler;

    Bool res = pHandler->GetPageContent(pPage, pageNo);
    if (!res) return false;

    // get default layer
    RenderDoc::CLayer *pLayer = pPage->getLayer(0);
    RenderDoc::CRdRect rect = pLayer->getElements()->rect;
    pLayer->setBbox(rect);
    pPage->rect = rect;

    return true;
}

void CSvgFile::Close()
{
    if (svgppHandler == NULL) return;

    delete (CSvgppHandler*)svgppHandler;
}
