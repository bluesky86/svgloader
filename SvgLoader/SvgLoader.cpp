// SvgLoader.cpp : Defines the exported functions for the DLL application.
//#include "stdafx.h"
#include "SvgLoader.h"
#include "types.h"
#include "RenderDoc.h"
#include "RdFile.h"
#include "Utils.h"
#include <Windows.h>

using namespace RenderDoc;

size_t reverseCharPos(Wchar *string, Wchar ch);

extern "C"
{
    static Wchar SvgLoader_Error[256];

    DLL_SO_EXPORT Wchar* GetLoaderLastError()
    {
        return SvgLoader_Error;
    }

    DLL_SO_EXPORT Bool Recognizer(Wchar *fileName)
    {
        RD_FILE *fp = rd_wfopen_2mem(fileName, L"rb");

        if (fp)
        {
            Bool res = RecognizerBuffer(fp->buffer, (S32)fp->filesize);
            rd_fclose(fp, true);
            return res;
        }

        return false;
    }

    DLL_SO_EXPORT Bool RecognizerBuffer(U8 *pData, S32 datalen)
    {
        //  You must have at least <svg in the file
        const int  kMinFileSize = 4;

        if (datalen >= kMinFileSize)
        {
            const int  SVGHdrSize = 2048;
            U8  *buffer = pData;

            size_t i = 0;   //index for buffer

            for (i = 0; i < SVGHdrSize;)
            {
                if (buffer[i] == '<')                //beginning of a tag
                {
                    i++;

                    if (buffer[i] == '!')             //check for "!DOCTYPE svg"
                    {
                        i++;

                        if (::memcmp("DOCTYPE", &buffer[i], 7) == 0)
                        {
                            //  Skip over DOCTYPE and advance one character.
                            i += 8;

                            //  Skip over any whitespace that might be present.
                            for (; i < SVGHdrSize && (buffer[i] == ' ' || buffer[i] == '\n' || buffer[i] == '\t' || buffer[i] == '\r');)
                                i++;

                            if (i < SVGHdrSize)
                            {
                                if (::memcmp("svg", &buffer[i], 3) == 0)
                                    return true;
                            }
                        }
                    }
                    else if (buffer[i] == 's')
                    {
                        //  Adobe SVG viewer and Batik can handle SVG files without a DOCTYPE declaration they only look for the <svg> element.
                        if (::memcmp("svg", &buffer[i], 3) == 0)
                            return true;
                    }

                    //not proper tag, search for end
                    while (buffer[i] != '>' && i < SVGHdrSize)
                        i++;
                }

                i++;
            }
        }

        return false;
    }

    DLL_SO_EXPORT S32 OpenDoc(RenderDoc::CRenderDoc* renderDoc)
    {
        CSvgLoader *pSvgLoader = new CSvgLoader();

        Bool res = pSvgLoader->svgFile.Open(renderDoc->fileName, L"rb");
        if (!res)
        {
            //wsprintf(SvgLoader_Error, L"File cannot be openned:\n\n%s", renderDoc->fileName);
            delete pSvgLoader;
            return 0;
        }

        renderDoc->hLoaderDocHandle = (Handle)pSvgLoader;

        return 1;
    }

    DLL_SO_EXPORT RenderDoc::CRdPage* GetPage(RenderDoc::CRenderDoc* renderDoc, S32 pageNo)
    {
        CSvgLoader *pSvgLoader = (CSvgLoader*)renderDoc->hLoaderDocHandle;

        //// create Render page
        CRdPage *pRdPage = new CRdPage();
        pSvgLoader->pRdPage = pRdPage;

        Bool res = pSvgLoader->svgFile.GetPage(0, pRdPage);
        if (res == false)
        {
            delete pRdPage;
            return NULL;
        }

        return pRdPage;
    }

    DLL_SO_EXPORT void CloseDoc(RenderDoc::CRenderDoc* renderDoc)
    {
        CSvgLoader *pSvgLoader = (CSvgLoader*)renderDoc->hLoaderDocHandle;
        if (pSvgLoader)
        {
            delete pSvgLoader;
        }
    }

};

size_t reverseCharPos(Wchar *string, Wchar ch, S32 startPos)
{
    size_t len = wcslen(string);
    if (startPos > len - 1)
        startPos = (S32)(len - 1);
    if (startPos <= 0)
        startPos = (S32)(len - 1);

    for (; startPos >= 0; startPos--)
    {
        if (string[startPos] == ch)
            break;
    }
    return startPos; // -1 if not found
}

void SetError(const Wchar *msg)
{
    wcscpy_s(SvgLoader_Error, 256, msg);
}

