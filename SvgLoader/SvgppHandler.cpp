//#include "stdafx.h"
#include "SvgppHandler.h"
#include "Matrix.h"
#include "Utils.h"

extern void SetError(const Wchar *msg);

char svg_doc_text[] = "<svg/>";

RenderDoc::CRdPage *g_SvgPage = NULL;
RenderDoc::CGroup *g_CurrentParent = NULL;
RenderDoc::CMatrix3x3 g_CurrentMatInv;
//RenderDoc::CPath *g_CurrentClipPath = NULL;
RenderDoc::CClipPath g_CurrentClipPath; 
RenderDoc::RD_CLIP_MODE g_CurrentClipMode = RenderDoc::RD_CLIP_COPY;

CSvgppHandler::CSvgppHandler()
    : m_pXmlDoc(0), m_pFileName(0)
{
}


CSvgppHandler::~CSvgppHandler()
{
    if (m_pXmlDoc)
    {
        delete m_pXmlDoc;
        m_pXmlDoc = NULL;
    }
    if (m_pFileName)
    {
        delete[] m_pFileName;
        m_pFileName = NULL;
    }
}

Bool CSvgppHandler::Open(const Wchar *fileName)
{
    return Open(RD_Wchar2Char(fileName));
}

Bool CSvgppHandler::Open(const char *fileName)
{
    if (fileName)
    {
        try
        {
            if (m_pXmlDoc) delete m_pXmlDoc;
            if (m_pFileName) delete[] m_pFileName;
            m_pFileName = (char*)fileName;
            m_pXmlDoc = new XMLDocument();
            m_pXmlDoc->load(fileName);

            return true;
        }
        catch (svgpp::exception_base const & e)
        {
            typedef boost::error_info<svgpp::tag::error_info::xml_element, XMLElement> element_error_info;
            if (XMLElement const * element = boost::get_error_info<element_error_info>(e))
            {
                char buf[256];
                sprintf_s(buf, 255, "Error reading file %s in element \"%s\": %s\n", m_pFileName, (*element)->name(), e.what());
                Wchar *error = RD_Char2Wchar(buf);
                SetError(error);
                delete[] error;
            }
            return false;
        }
        catch (std::exception const & e)
        {
            char buf[256];
            sprintf_s(buf, 255, "Error reading file %s: %s\n", m_pFileName, e.what());
            Wchar *error = RD_Char2Wchar(buf);
            SetError(error);
            delete[] error;
            return false;
        }
    }

    return false;
}

Bool CSvgppHandler::Open(const char *pData, S32 buflen)
{
    if (pData && buflen > 0)
    {
        try
        {
            if (m_pXmlDoc) delete m_pXmlDoc;
            if (m_pFileName) delete[] m_pFileName;
            m_pFileName = NULL;
            m_pXmlDoc = new XMLDocument();
            m_pXmlDoc->load(pData, buflen);

            return true;
        }
        catch (svgpp::exception_base const & e)
        {
            typedef boost::error_info<svgpp::tag::error_info::xml_element, XMLElement> element_error_info;
            if (XMLElement const * element = boost::get_error_info<element_error_info>(e))
            {
                char buf[256];
                sprintf_s(buf, 255, "Error reading file %s in element \"%s\": %s\n", m_pFileName, (*element)->name(), e.what());
                Wchar *error = RD_Char2Wchar(buf);
                SetError(error);
                delete[] error;
            }
            return false;
        }
        catch (std::exception const & e)
        {
            char buf[256];
            sprintf_s(buf, 255, "Error reading file %s: %s\n", m_pFileName, e.what());
            Wchar *error = RD_Char2Wchar(buf);
            SetError(error);
            delete[] error;
            return false;
        }
    }

    return false;
}


Bool CSvgppHandler::GetPageContent(RenderDoc::CRdPage *pPage, int pageNo)
{
    if (pPage == NULL) return false;

    // refresh the object of Render page
    g_SvgPage = pPage;
    g_CurrentParent = pPage->getLayer(0)->getElements();

    // parsing svg file
    try
    {
        ImageBuffer buffer;
        Document document(*m_pXmlDoc);
        Canvas canvas(document, buffer);
        document_traversal_main::load_document(m_pXmlDoc->getRoot(), canvas);
    }
    catch (svgpp::exception_base const & e)
    {
        typedef boost::error_info<svgpp::tag::error_info::xml_element, XMLElement> element_error_info;
        if (XMLElement const * element = boost::get_error_info<element_error_info>(e))
        {
            char buf[256];
            sprintf_s(buf, 255, "Error reading file %s in element \"%s\": %s\n", m_pFileName, (*element)->name(), e.what());
            Wchar *error = RD_Char2Wchar(buf);
            SetError(error);
            delete[] error;
        }
        return false;
    }
    catch (std::exception const & e)
    {
        char buf[256];
        sprintf_s(buf, 255, "Error reading file %s: %s\n", m_pFileName, e.what());
        Wchar *error = RD_Char2Wchar(buf);
        SetError(error);
        delete[] error;
        return false;
    }

    return true;
}
