#pragma once
#include "types.h"
#include "RenderDoc.h"
#include "RdFile.h"

class CSvgFile
{
public:
    CSvgFile();
    ~CSvgFile();

    Bool Open(Wchar *filename, const Wchar *mode);
    Bool Open(U8 *pData, size_t bufLen);
    void Close();

public:
    RenderDoc::RD_FILE *file_ptr;
    void *svgppHandler;
};

