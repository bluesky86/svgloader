//#include "stdafx.h"
#include "SvgppHandler.h"
#include "Matrix.h"


namespace
{
    template<class GrayMask>
    void blend_image_with_mask(boost::gil::rgba8_view_t const & rgbaView, GrayMask const & maskView)
    {
        boost::gil::rgba8_view_t::iterator o = rgbaView.begin();
        for (typename GrayMask::iterator m = maskView.begin(); m != maskView.end(); ++m, ++o)
        {
            using namespace boost::gil;
            get_color(*o, alpha_t()) =
                channel_multiply(
                get_color(*o, alpha_t()),
                get_color(*m, gray_color_t())
                );
        }
        BOOST_ASSERT(o == rgbaView.end());
    }
}

Canvas::Canvas(Document & document)
    : document_(document)
    , image_buffer_(NULL)
{
}

Canvas::Canvas(Document & document, ImageBuffer & image_buffer)
    : document_(document)
    , parent_buffer_(boost::bind(&Canvas::getPassedImageBuffer, this))
    , image_buffer_(&image_buffer)
    , rendering_disabled_(false)
{
    if (image_buffer.isSizeSet())
        clip_buffer_.reset(new ClipBuffer(image_buffer_->width(), image_buffer_->height()));
}

Canvas::Canvas(Canvas & parent)
    : Transformable(parent)
    , Stylable(parent)
    , document_(parent.document_)
    , image_buffer_(NULL)
    , parent_buffer_(boost::bind(&Canvas::getImageBuffer, &parent))
    , length_factory_(parent.length_factory_)
    , clip_buffer_(parent.clip_buffer_)
    , rendering_disabled_(false)
{}

Canvas::Canvas(Canvas & parent, dontInheritStyle)
    : Transformable(parent)
    , document_(parent.document_)
    , image_buffer_(NULL)
    , parent_buffer_(boost::bind(&Canvas::getImageBuffer, &parent))
    , length_factory_(parent.length_factory_)
    , clip_buffer_(parent.clip_buffer_)
    , rendering_disabled_(false)
{}

void Canvas::on_exit_element()
{
    if (!own_buffer_.get())
        return;
    applyFilter();

    if (style().clip_path_fragment_)
    {
        if (!clip_buffer_.unique())
            clip_buffer_.reset(new ClipBuffer(*clip_buffer_));
        clip_buffer_->intersectClipPath(document().xml_document_, *style().clip_path_fragment_, transform());
    }

    if (clip_buffer_)
        blend_image_with_mask(own_buffer_->gilView(), clip_buffer_->gilView());

    if (style().mask_fragment_)
    {
        ImageBuffer & parent_buffer = parent_buffer_();
        ImageBuffer mask_buffer(parent_buffer.width(), parent_buffer.height());
        loadMask(mask_buffer);
        typedef boost::gil::color_converted_view_type<
            boost::gil::rgba8_view_t,
            boost::gil::gray8_pixel_t,
            svgpp::gil_utility::rgba_to_mask_color_converter<>
        >::type mask_view_t;
        mask_view_t mask_view = boost::gil::color_converted_view<boost::gil::gray8_pixel_t>(
            mask_buffer.gilView(), svgpp::gil_utility::rgba_to_mask_color_converter<>());

        blend_image_with_mask(own_buffer_->gilView(), mask_view);
    }
    {
        Gdiplus::ColorMatrix color_matrix[] = {
            1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, style().opacity_, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f, 1.0f };
        Gdiplus::ImageAttributes ImgAttr;
        ImgAttr.SetColorMatrix(color_matrix, Gdiplus::ColorMatrixFlagsDefault, Gdiplus::ColorAdjustTypeBitmap);
        Gdiplus::Graphics graphics(&parent_buffer_().bitmap());
        graphics.DrawImage(
            &own_buffer_->bitmap(),
            Gdiplus::Rect(0, 0, own_buffer_->width(), own_buffer_->height()),
            0, 0, own_buffer_->width(), own_buffer_->height(),
            Gdiplus::UnitPixel, &ImgAttr);
    }
}

void Canvas::set_viewport(number_t viewport_x, number_t viewport_y, number_t viewport_width, number_t viewport_height)
{
    if (image_buffer_) // If topmost SVG element
    {
        image_buffer_->setSize(viewport_width + 1.0, viewport_height + 1.0, TransparentWhiteColor());
        clip_buffer_.reset(new ClipBuffer(image_buffer_->width(), image_buffer_->height()));
    }
    else
    {
        if (style().overflow_clip_)
        {
            if (!clip_buffer_.unique())
                clip_buffer_.reset(new ClipBuffer(*clip_buffer_));
            clip_buffer_->intersectClipRect(transform(), viewport_x, viewport_y, viewport_width, viewport_height);
        }
    }
    length_factory_.set_viewport_size(viewport_width, viewport_height);
}

void Canvas::set_viewbox_size(number_t viewbox_width, number_t viewbox_height)
{
    // If "viewbox" attribute is present, then percentage is calculated relative to viewbox
    length_factory_.set_viewport_size(viewbox_width, viewbox_height);
}

void Canvas::disable_rendering()
{
    rendering_disabled_ = true;
}

bool Canvas::rendering_disabled() const
{
    return rendering_disabled_;
}

length_factory_t & Canvas::length_factory()
{
    return length_factory_;
}

length_factory_t const & Canvas::length_factory() const
{
    return length_factory_;
}

void Canvas::applyFilter()
{
    if (!style().filter_)
        return;

    Filters::Input in;
    in.sourceGraphic_ = IFilterViewPtr(new SimpleFilterView(own_buffer_->gilView()));
    in.backgroundImage_ = IFilterViewPtr(new SimpleFilterView(parent_buffer_().gilView()));
    IFilterViewPtr out = document_.filters_.get(*style().filter_, length_factory_, in);
    if (out)
        boost::gil::copy_pixels(out->view(), own_buffer_->gilView());
}

void Canvas::loadMask(ImageBuffer & mask_buffer) const
{
    if (XMLElement element = document().xml_document_.findElementById(*style().mask_fragment_))
    {
        Document::FollowRef lock(document(), element);

        Mask mask(document_, mask_buffer, *this);
        document_traversal_main::load_expected_element(element, mask, svgpp::tag::element::mask());
    }
    else
        throw std::runtime_error("Element referenced by 'mask' not found");
}

ImageBuffer & Canvas::getImageBuffer()
{
    ImageBuffer & parent_buffer = parent_buffer_();

    if (style().opacity_ < 0.999
        || style().mask_fragment_
        || style().clip_path_fragment_
        || style().filter_)
    {
        if (!own_buffer_.get())
            own_buffer_.reset(new ImageBuffer(parent_buffer.width(), parent_buffer.height()));
        return *own_buffer_;
    }
    return parent_buffer;
}


void Path::on_exit_element()
{
    if (style().display_)
    {
        drawPath();
        drawMarkers();
    }
    Canvas::on_exit_element();
}

void Path::marker(svgpp::marker_vertex v, number_t x, number_t y, number_t directionality, unsigned marker_index)
{
    if (marker_index >= markers_.size())
        markers_.resize(marker_index + 1);
    MarkerPos & m = markers_[marker_index];
    m.v = v;
    m.x = x;
    m.y = y;
    m.directionality = directionality;
}

boost::optional<svg_string_t> & Path::getMarkerReference(svgpp::marker_vertex v)
{
    switch (v)
    {
    default:
        BOOST_ASSERT(false);
    case svgpp::marker_start:
        return style().marker_start_;
    case svgpp::marker_mid:
        return style().marker_mid_;
    case svgpp::marker_end:
        return style().marker_end_;
    }
}

void Path::drawPath()
{
    //if (path_points_.empty())
    //  return;
    //Gdiplus::Graphics graphics(&getImageBuffer().bitmap());
    //graphics.SetSmoothingMode(Gdiplus::SmoothingModeHighQuality);
    //graphics.SetTransform(&transform());
    //Gdiplus::GraphicsPath path(&path_points_[0], &path_types_[0], path_types_.size(), 
    //  style().nonzero_fill_rule_ ? Gdiplus::FillModeWinding : Gdiplus::FillModeAlternate);
    //EffectivePaint fill = getEffectivePaint(style().fill_paint_);
    //if (boost::get<svgpp::tag::value::none>(&fill) == NULL)
    //{
    //if (color_t const * color = boost::get<color_t>(&fill))
    //  graphics.FillPath(&Gdiplus::SolidBrush(Gdiplus::Color(style().fill_opacity_ * 255, 
    //    color->GetR(), color->GetG(), color->GetB())), &path);
    // TODO: gradient
    //}
    //EffectivePaint stroke = getEffectivePaint(style().stroke_paint_);
    //if (boost::get<svgpp::tag::value::none>(&stroke) == NULL)
    //{
    //  if (color_t const * color = boost::get<color_t>(&stroke))
    //  {
    //Gdiplus::Pen pen(Gdiplus::Color(style().stroke_opacity_ * 255, 
    //    color->GetR(), color->GetG(), color->GetB()), 
    //  style().stroke_width_);
    //pen.SetStartCap(style().line_cap_);
    //pen.SetEndCap(style().line_cap_);
    //pen.SetLineJoin(style().line_join_);
    //pen.SetMiterLimit(style().miterlimit_);
    //std::vector<number_t> const & dasharray = style().stroke_dasharray_;
    //if (!dasharray.empty())
    //{
    //  std::vector<Gdiplus::REAL> dashes(dasharray.begin(), dasharray.end());
    //  if (dasharray.size() % 2 == 1)
    //    dashes.insert(dashes.end(), dasharray.begin(), dasharray.end());
    //  pen.SetDashPattern(&dashes[0], dashes.size());
    //  pen.SetDashOffset(style().stroke_dashoffset_);
    //}
    //graphics.DrawPath(&pen, &path);
    //  }
    //  // TODO: gradient
    //}
}

void Path::drawMarkers()
{
    if (!style().marker_start_ && !style().marker_mid_ && !style().marker_end_)
        return;
    for (Markers::const_iterator pos = markers_.begin(); pos != markers_.end(); ++pos)
    {
        if (boost::optional<svg_string_t> & m = getMarkerReference(pos->v))
        {
            drawMarker(*m, pos->x, pos->y, pos->directionality);
        }
    }
}

void Path::drawMarker(svg_string_t const & id, number_t x, number_t y, number_t dir)
{
    if (XMLElement element = document().xml_document_.findElementById(id))
    {
        Document::FollowRef lock(document(), element);

        Marker markerContext(*this,
            style().stroke_width_,
            x, y, dir);
        document_traversal_main::load_expected_element(element, markerContext, svgpp::tag::element::marker());
    }
}

Path::EffectivePaint Path::getEffectivePaint(Paint const & paint) const
{
    SolidPaint const * solidPaint = NULL;
    if (IRIPaint const * iri = boost::get<IRIPaint>(&paint))
    {
        if (boost::optional<Gradient> const gradient = document().gradients_.get(iri->fragment_, length_factory()))
        {
            GradientBase_visitor gradientBase;
            boost::apply_visitor(gradientBase, *gradient);
            if (gradientBase.gradient_->stops_.empty())
                return svgpp::tag::value::none();
            if (gradientBase.gradient_->stops_.size() == 1)
                return gradientBase.gradient_->stops_.front().color_;
            if (LinearGradient const * linearGradient = boost::get<LinearGradient>(gradient.get_ptr()))
            {
                if (linearGradient->x1_ == linearGradient->x2_ && linearGradient->y1_ == linearGradient->y2_)
                    // TODO: use also last step opacity 
                    return gradientBase.gradient_->stops_.back().color_;
            }
            return *gradient;
        }
        if (iri->fallback_)
            solidPaint = &*iri->fallback_;
        else
            throw std::runtime_error("Can't find paint server");
    }
    else
        solidPaint = boost::get<SolidPaint>(&paint);
    if (boost::get<svgpp::tag::value::none>(solidPaint))
        return svgpp::tag::value::none();
    if (boost::get<svgpp::tag::value::currentColor>(solidPaint))
        return style().color_;
    return boost::get<color_t>(*solidPaint);
}


Use::Use(Canvas & parent)
    : Canvas(parent)
    , x_(0)
    , y_(0)
{}

void Use::on_exit_element()
{
    if (!style().display_)
        return;
    if (XMLElement element = document().xml_document_.findElementById(fragment_id_))
    {
        Document::FollowRef lock(document(), element);
        //transform().Translate(x_, y_);
        transform()[4] += x_;
        transform()[5] += y_;
        document_traversal_main::load_referenced_element<
            svgpp::referencing_element<svgpp::tag::element::use_>,
            svgpp::expected_elements<svgpp::traits::reusable_elements>,
            svgpp::processed_elements<
            boost::mpl::insert<processed_elements, svgpp::tag::element::symbol>::type
            >
        >::load(element, *this);
    }
    else
        std::cerr << "Element referenced by 'use' not found\n";
    Canvas::on_exit_element();
}


void renderDocument(XMLDocument & xmlDocument, ImageBuffer & buffer)
{
    Document document(xmlDocument);
    Canvas canvas(document, buffer);
    document_traversal_main::load_document(xmlDocument.getRoot(), canvas);
}

int main_orig(int argc, char * argv[])
{
    if (argc < 2)
    {
        std::cout << "Usage: " << argv[0] << " <svg file name> [<output BMP file name>]\n";
        return 1;
    }

    Gdiplus::GdiplusStartupInput gdiplusStartupInput;
    ULONG_PTR gdiplusToken;
    Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

    {
        ImageBuffer buffer;

        XMLDocument xmlDoc;
        try
        {
            xmlDoc.load(argv[1]);
            renderDocument(xmlDoc, buffer);
        }
        catch (svgpp::exception_base const & e)
        {
            typedef boost::error_info<svgpp::tag::error_info::xml_element, XMLElement> element_error_info;
            std::cerr << "Error reading file " << argv[1];
#if defined(SVG_PARSER_RAPIDXML_NS)
            if (XMLElement const * element = boost::get_error_info<element_error_info>(e))
                std::cerr << " in element \"" << std::string((*element)->name(), (*element)->name() + (*element)->name_size())
                << "\"";
#endif
            std::cerr << ": " << e.what() << "\n";
        }
        catch (std::exception const & e)
        {
            std::cerr << "Error reading file " << argv[1] << ": " << e.what() << "\n";
            return 1;
        }

        // Saving output
        const char * out_file_name = argc > 2 ? argv[2] : "svgpp.png";
        // {C2510B29-9212-4866-A354-6EA79710636C}
        static const GUID PNGEncoderCLSID =
        { 0x557cf406, 0x1a04, 0x11d3, { 0x9a, 0x73, 0x00, 0x00, 0xf8, 0x1e, 0xf3, 0x2e } };
        buffer.bitmap().Save(std::wstring(out_file_name, out_file_name + strlen(out_file_name)).c_str(),
            &PNGEncoderCLSID, NULL);
    }
    Gdiplus::GdiplusShutdown(gdiplusToken);

    return 0;
}

