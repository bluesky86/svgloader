//#include "stdafx.h"
#include "SvgFile.h"
#include "SvgppHandler.h"

extern void SetError(const Wchar *msg);

CSvgFile::CSvgFile()
{
    svgppHandler = NULL;
}


CSvgFile::~CSvgFile()
{
}

Bool CSvgFile::Open(Wchar *filename, const Wchar *mode)
{
    file_ptr = RenderDoc::rd_wfopen(filename, mode, NULL, 0);
    if (file_ptr) return true;
    SetError(L"Failed to open png file. The file might not exist!");
    return false;
}

Bool CSvgFile::Open(U8 *pData, size_t bufLen)
{
    if (bufLen && pData)
    {
        file_ptr = RenderDoc::rd_wfopen(NULL, NULL, pData, bufLen);
        if (file_ptr) return true;
    }
    SetError(L"Failed to read png from buffer!");
    return false;
}


void CSvgFile::Close()
{

}
