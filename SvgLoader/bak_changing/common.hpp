#pragma once

//#define SVG_PARSER_RAPIDXML_NS
#undef SVG_PARSER_MSXML

#define BOOST_PARAMETER_MAX_ARITY 17
// Following defines move parts of SVG++ code to svgpp_parser_impl.cpp file
// reducing compiler memory requirements
#define SVGPP_USE_EXTERNAL_PATH_DATA_PARSER
#define SVGPP_USE_EXTERNAL_TRANSFORM_PARSER
#define SVGPP_USE_EXTERNAL_PRESERVE_ASPECT_RATIO_PARSER
#define SVGPP_USE_EXTERNAL_PAINT_PARSER
#define SVGPP_USE_EXTERNAL_MISC_PARSER
#define SVGPP_USE_EXTERNAL_COLOR_PARSER
#define SVGPP_USE_EXTERNAL_LENGTH_PARSER

#include <windows.h>
#include <gdiplus.h>
#undef min
#undef max
#undef small
#include <vector>

#include <svgpp/factory/integer_color.hpp>
#include <svgpp/factory/unitless_length.hpp>
#if defined(SVG_PARSER_MSXML)
# include "parser_msxml.hpp"
#elif defined(SVG_PARSER_RAPIDXML_NS)
# include "parser_rapidxml_ns.hpp"
#elif defined(SVG_PARSER_LIBXML)
# include "parser_libxml.hpp"
#elif defined(SVG_PARSER_XERCES)
# include "parser_xerces.hpp"
#else
#error One of XML parsers must be set with SVG_PARSER_xxxx macro
#endif
#include <boost/function.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/array.hpp>
#include <map>

#include "types.h"
#include "RenderDoc.h"
#include "Matrix.h"
#include "RdFile.h"


typedef Double number_t;
typedef boost::array<Double, 6> transform_t;
typedef U32 color_t;

// ARGB
inline U8 GetR(color_t c) { return (c >> 16) && 0xFF; };
inline U8 GetG(color_t c) { return (c >> 8) && 0xFF; };
inline U8 GetB(color_t c) { return (c & 0xFF); };
inline U8 GetA(color_t c) { return (c >> 24); };
inline color_t CreateColor(U8 r, U8 g, U8 b, U8 a) { return (a << 24) + (r << 16) + (g << 8) + b; };
inline color_t BlackColor() { return 0; }
inline color_t TransparentBlackColor() { return 0; }
inline color_t TransparentWhiteColor() { return CreateColor(255, 255, 255, 0); };

struct color_factory_base_t
{
    typedef U32 color_type;

    static color_type create(unsigned char r, unsigned char g, unsigned char b)
    {
        return ((r << 16) + (g << 8) + b);
    }
};
typedef svgpp::factory::color::percentage_adapter<color_factory_base_t> color_factory_t;

typedef svgpp::factory::length::unitless<number_t, number_t> length_factory_t;
typedef boost::tuple<double, double, double, double> bounding_box_t;
typedef boost::function<bounding_box_t()> get_bounding_box_func_t;

inline void AssignMatrix(transform_t & dest, transform_t const & src)
{
    for (int i = 0; i < 6; i++)
        dest[i] = src[i];
    //Gdiplus::REAL m[6];
    //src.GetElements(m);
    //dest.SetElements(m[0], m[1], m[2], m[3], m[4], m[5]);
}

class PathStorage
{
public:
    void path_move_to(number_t x, number_t y, svgpp::tag::coordinate::absolute const &)
    {
        path_points_.push_back(RenderDoc::CRealPoint((Float)x, (Float)y));
        path_types_.push_back(RenderDoc::RDPATH_MOVETO);
    }

    void path_line_to(number_t x, number_t y, svgpp::tag::coordinate::absolute const &)
    {
        path_points_.push_back(RenderDoc::CRealPoint((Float)x, (Float)y));
        path_types_.push_back(RenderDoc::RDPATH_LINETO);
    }

    void path_cubic_bezier_to(
        number_t x1, number_t y1,
        number_t x2, number_t y2,
        number_t x, number_t y,
        svgpp::tag::coordinate::absolute const &)
    {
        // TODO:
        path_points_.push_back(RenderDoc::CRealPoint((Float)x1, (Float)y1));
        path_types_.push_back(RenderDoc::RDPATH_BEZIERTO);
        path_points_.push_back(RenderDoc::CRealPoint((Float)x2, (Float)y2));
        path_types_.push_back(RenderDoc::RDPATH_BEZIERTO);
        path_points_.push_back(RenderDoc::CRealPoint((Float)x, (Float)y));
        path_types_.push_back(RenderDoc::RDPATH_BEZIERTO);
    }

    void path_close_subpath()
    {
        if (!path_types_.empty())
        {
            //path_types_.back() |= RenderDoc::RDPATH_CLOSE_PATH;
            path_points_.push_back(path_points_.front());
            path_types_.push_back(RenderDoc::RDPATH_LINETO);
        }
    }

    void path_exit()
    {}

    std::vector<RenderDoc::CRealPoint> path_points_;
    std::vector<RenderDoc::RD_PATH_OP> path_types_;
};

